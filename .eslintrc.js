module.exports = {
  extends: ['airbnb-base', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',

    // Same as AirBnB, but adds `ret` to exclusions
    // rule: https://eslint.org/docs/rules/no-param-reassign.html
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'acc', // for reduce accumulators
          'accumulator', // for reduce accumulators
          'e', // for e.returnvalue
          'ctx', // for Koa routing
          'req', // for Express requests
          'request', // for Express requests
          'res', // for Express responses
          'ret', // for Mongoose expressions.
          'response', // for Express responses
          '$scope', // for Angular 1 scopes
        ],
      },
    ],

    // Same as AirBnB, but adds `_id` and `__v` to exclusions
    // rule: https://eslint.org/docs/rules/no-param-reassign.html
    'no-underscore-dangle': [
      'error',
      {
        allow: ['_id', '__v'], // Respectively mongoose magic keys for id and schema version.
      },
    ],
  },
  env: {
    node: true,
    jest: true,
  },
};
