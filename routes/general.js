/**
 * Defines overall rules for routes.
 */

// Imports.
const router = require('express-promise-router')();

/**
 * GET / health checkpoint.
 */
router.get('/', (req, res) => {
  res.send({
    status: 'running',
    version: 'v2',
  });
});

module.exports = router;
