// Imports.
const Joi = require('joi');
const router = require('express-promise-router')();
const logger = require('../../services/logger');
const Bookmark = require('../../models/bookmark');
const { extractCoverImageForBookmark } = require('../../services/fetch');

/**
 * Returns a cover illustration for the given URL.
 *
 * The input requires URL parameters as follow:
 *  - bid: the bookmark ID
 *  - rid: the resource ID
 *  - height: the desired cover height
 *  - width: the desired cover width
 */
router.get('/cover', async (req, res) => {
  const validationSchema = Joi.object()
    .keys({
      bid: Joi.string()
        .regex(/[0-9a-fA-F]{24}/)
        .required(),
      rid: Joi.string()
        .regex(/[0-9a-fA-F]{24}/)
        .required(),
      width: Joi.number().integer().min(0).max(2048),
      height: Joi.number().integer().min(0).max(2048),
    })
    .and('width', 'height');

  // Validate the user inputs.
  const { error } = Joi.validate(req.query, validationSchema);
  if (error) {
    logger.debug(`[CoverFinderService] ${error.details[0].message}`);
    const err = new Error(error.details[0].message);
    err.statusCode = 400;
    throw err;
  }

  // Validates the input data by fetching the corresponding bookmark and resource.
  const { bid, rid } = req.query;
  const bookmark = await Bookmark.findOne({ _id: bid, rid }).populate('rid');
  if (!bookmark || !bookmark.rid || !bookmark.rid.cacheDir) {
    return res.status(404).send();
  }

  // Find the bookmark url (either from query or from DB).
  const expectedWidth = req.query.width || 70;
  const expectedHeight = req.query.height || 70;
  const filePath = await extractCoverImageForBookmark(bookmark, expectedWidth, expectedHeight);

  // Return the cover illustration file.
  if (filePath) return res.sendFile(filePath);
  return res.status(404).send();
});

module.exports = router;
