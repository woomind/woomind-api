/**
 * Defines CRUD operations on User entity.
 *
 * Note: all /user/me routes referes to the requester user.
 * The requester is self-identified via the AccessToken header used to query the route.
 * It is sent via Bearer header (Authorize) and validated at previous step from the Passport Bearer strategy (@see /middleware/auth.js).
 */

// Imports.
const router = require('express-promise-router')();
const User = require('../../models/user');
const auth = require('../../middlewares/auth');
const logger = require('../../services/logger');
const { EntityNotFoundError } = require('../../utils/errors');

//  == HELPERS ==

/**
 * Helper method:
 * Update profile information for the given user.
 *
 * @param uid (string) the user ID to update
 * @param input (object) the user input: data to update user with.
 * @param context (object) update contexts.
 * @return (User) the user updated
 * @throws err in case of error.
 *  err.statusCode: the REST status code to return for this error
 *  err.message: an error message.
 */
async function updateUser(uid, input, context = { patch: false }) {
  const data = input; // Just to please linter :)

  logger.logComplexData(data, `[UpdateUser] Try to update user ${uid} with following data:`);

  // Perform all verification check concurrently for better performance.
  await Promise.all([
    // Validate against JOI schema.
    User.checkInputValidity(data, context),
    // Validate password strength.
    context.patch && !data.password ? null : User.checkPasswordStrength(data.password),
  ]);

  // Update user.
  const updatedUser = await User.update(uid, data);
  if (!updatedUser) {
    throw new EntityNotFoundError('User', { uid });
  }
  return updatedUser;
}

//  == REST ==

/**
 * POST /users/register
 *
 * Registers a new user.
 * See validate() in models/user.js for allowed request input.
 */
router.post('/register', auth.isClientAuthentified, async (req, res) => {
  logger.debug('[UserRegister] Perform check operations.');
  logger.logComplexData(req.body, '[UserRegister] User input data', 'silly');

  // Perform all verification check concurrently for better performance.
  await Promise.all([
    // Validate against JOI schema.
    User.checkInputValidity(req.body),
    // Validate password strength.
    User.checkPasswordStrength(req.body.password),
    // Validate user uniqueness.
    User.checkExists({ email: req.body.email }, false),
  ]);

  // Build the new user.
  let newUser = new User(req.body);
  logger.logComplexData(newUser.toObject(), '[UserRegister] New user data', 'silly');

  // Save the new user.
  logger.silly('[UserRegister] Save user in storage.');
  try {
    newUser = await newUser.save();
  } catch (e) {
    logger.silly(e);
  }

  // Respond.
  res.send({ data: newUser });
});

/**
 * GET /user/me
 *
 * Get's information about the requester user.
 */
router.get('/me', auth.isUserAuthentified, auth.isUserAllowed(['user:read']), (req, res) => {
  res.send({ data: req.user });
});

/**
 * GET /users/:uid
 *
 * Get's information about the authenticated user.
 */
router.get(
  '/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified, // Client must be trusted.
  auth.isUserAuthentified, // User must be authenticated
  auth.isUserAllowed(['users:read']), // User must be allow to read other users data.
  async (req, res) => {
    const user = await User.checkExists(req.params.uid);
    return res.send({ data: user });
  }
);

/**
 * PUT /users/me
 *
 * Replace profile information about the authenticated user.
 * This update method only accepts full object update.
 */
router.put('/me', auth.isUserAuthentified, auth.isUserAllowed(['user:all']), async (req, res) => {
  const updatedUser = await updateUser(req.user.uid, req.body);
  return res.send({ data: updatedUser });
});

/**
 * PUT /users/:uid
 *
 * Replace profile information for the given user id.
 * This update method only accepts full object update
 */
router.put(
  '/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['users:all']),
  async (req, res) => {
    const updatedUser = await updateUser(req.params.uid, req.body);
    return res.send({ data: updatedUser });
  }
);

/**
 * PATCH /users/me
 *
 * Update profile information about the authenticated user.
 * This update method accepts partial updates.
 */
router.patch('/me', auth.isUserAuthentified, auth.isUserAllowed(['user:all']), async (req, res) => {
  // Note: req.user is provided by auth middleware.
  const updatedUser = await updateUser(req.user.uid, req.body, { patch: true });
  return res.send({ data: updatedUser });
});

/**
 * PATCH /:uid
 *
 * Update profile information for the given user id.
 * This update method accepts partial updates.
 */
router.patch(
  '/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['users:all']),
  async (req, res) => {
    const updatedUser = await updateUser(req.params.uid, req.body, { patch: true });
    return res.send({ data: updatedUser });
  }
);

/**
 * DELETE /users/me
 *
 * Delete current user.
 */
router.delete(
  '/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['user:all']),
  async (req, res) => {
    const deleteUser = await User.delete(req.user.uid);
    if (!deleteUser) {
      throw new EntityNotFoundError('User');
    }
    return res.send({ data: deleteUser });
  }
);

/**
 * DELETE /users/:uid
 *
 * Delete given user.
 */
router.delete(
  '/:uid([0-9a-fA-F]{24})',
  auth.isUserAuthentified,
  auth.isUserAllowed(['users:all']),
  async (req, res) => {
    const deleteUser = await User.delete(req.params.uid);
    if (!deleteUser) {
      throw new EntityNotFoundError('User', { uid: req.params.uid });
    }
    return res.send({ data: deleteUser });
  }
);

module.exports = router;
