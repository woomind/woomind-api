/**
 * Defines routes for OAUth2 authentication.
 */

// Imports.
const config = require('config');
const oauth2orize = require('oauth2orize');
const router = require('express-promise-router')();
const logger = require('../../services/logger');
const User = require('../../models/user');
const AccessToken = require('../../models/access_token');
const utils = require('../../utils/generator');
const auth = require('../../middlewares/auth');

const oauth2Server = oauth2orize.createServer();

// Exchange user id and password for access tokens. The callback accepts the
// `client`, which is exchanging the user's name and password from the
// authorization request for verification. If these values are validated, the
// application issues an access token on behalf of the user who authorized the code.
oauth2Server.exchange(
  oauth2orize.exchange.password(async (client, username, password, scope, done) => {
    logger.debug(
      '[OAuth2 Password Grant] Requesting access token using Password Grant type strategy by authorized client.'
    );

    try {
      // Check client grants.
      if (client.grants.indexOf('password') < 0) {
        logger.debug(
          '[OAuth2 Password Grant] Requesting client is not authorized to use this strategy.'
        );
        return done(
          new oauth2orize.TokenError(
            'The Password grant type is unauthorized for this client.',
            'invalid_grant'
          )
        );
      }

      // Check user existence.
      const user = await User.findOne({ email: username });
      if (!user) {
        logger.debug(`[OAuth2 Password Grant] User not found with email: ${username}`);
        return done(null, false);
      }

      // Check scopes requester want to get an access token for. The purpose is to not issue an access token for
      // something the user can't do.
      const scopes = scope || ['unauthorized'];
      if (!auth.verifyScopes(user.scope, scopes)) {
        logger.debug(`[OAuth2 Password Grant] User scopes are not correct.`);
        return done(null, false);
      }

      if (!user.passwordMatch(password)) {
        logger.debug(`[OAuth2 Password Grant] User password is not correct.`);
        return done(null, false);
      }

      // Generate tokens (access and refresh).
      // @todo Should we encrypt our token ? For security purpose this could be could, specially for long term tokens
      // like refreshTokens. Not sure if required though. Also not sure about performan impact for accessTokens used
      // at every request.
      const accessToken = utils.generateToken(256);

      // The Access Token lifetime is set in the application config file.
      // @see /config folder.
      const expirationDate = new Date(
        new Date().getTime() + config.get('security.access_token_lifetime')
      );

      await new AccessToken({
        token: accessToken,
        expirationDate,
        clientId: client.clientId,
        userId: user.id,
        scope,
      }).save();

      return done(null, accessToken);
    } catch (err) {
      logger.logComplexData(err, `[OAuth2 Password Grant] Something went wrong.`, 'debug');
      return done(err);
    }
  })
);

/**
 * POST /oauth/token
 *
 * Authenticates an existing a user and gives him an AccessToken/RequestToken.
 */
router.post('/token', auth.isClientAuthentified, oauth2Server.token(), oauth2Server.errorHandler());

/**
 * GET /oauth/revoke
 *
 * Invalidates user tokens.
 *
 * If the token is valid you get returned a 200 and an empty object
 * {}
 * If the token is not valid you get a 400 Status and this returned
 * {
 *   "error": "invalid_token"
 * }
 */
router.get('/revoke/token', auth.isClientAuthentified, async (req, res) => {
  logger.debug('[OAuth Rewoke Token] Access token is asked to be removed.');

  let token = await AccessToken.findOneAndDelete({ token: req.authInfo.token });
  if (!token) {
    token = await AccessToken.findOneAndDelete({ token: req.authInfo.token });
  }

  if (!token) {
    logger.debug('[OAuth Rewoke Token] Provided token is not a valid token.');
    res.status(400);
    res.send({ error: 'invalid_token' });
  }

  res.send({});
});

module.exports = router;
