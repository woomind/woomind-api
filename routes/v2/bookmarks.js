/**
 * Defines routes for CRUD operations on Bookmark entity.
 */

// Imports.
const router = require('express-promise-router')();
const sanitize = require('sanitize-filename');
const mongoose = require('mongoose');
const multer = require('multer');
const fileParser = require('bookmarks-import-export');
const flattenTree = require('tree-flatten');
const searchQueryParser = require('search-query-parser');
const auth = require('../../middlewares/auth');

const Bookmark = require('../../models/bookmark');
const Resource = require('../../models/resource');
const User = require('../../models/user');
const logger = require('../../services/logger');
const fetchService = require('../../services/fetch');
const fileSystem = require('../../utils/fileSystem');
const { EntityNotFoundError, FileTypeError, FileParsingError } = require('../../utils/errors');
const ImportQueue = require('../../utils/importQueue');
const { addTask, getTask, removeTask } = require('../../utils/tasks.js');

//  == HELPERS ==

/**
 * Creates or updates a resource data.
 *
 * @param input
 * @param usage
 * @returns Resource
 */
async function createOrUpdateResource(input, usage) {
  const newId = new mongoose.Types.ObjectId();
  let data = {};
  let cacheDir = '';

  logger.debug(`[CreateUpdateResource] Update or create resource.`);
  logger.logComplexData(
    input,
    `[CreateUpdateResource] Update or create resource with following input: `,
    'silly'
  );

  // If adding: let's update the resource.
  if (usage >= 0) {
    // Compute the cache path.
    const date = new Date();
    const creationDate = `${date.getFullYear()}-${`0${date.getMonth() + 1}`.slice(-2)}`;
    cacheDir = `${fileSystem.getResourcesFolder()}/${creationDate}/${sanitize(
      input.domain
    )}/${newId}`;
    // Build the data.
    data = {
      url: input.url,
      title: input.title,
      description: input.description,
      favicon: input.favicon,
    };
  }

  // Build filter query.
  const filterQuery = input.rid ? { _id: input.rid } : { url: input.url };

  logger.logComplexData(
    data,
    `[CreateUpdateResource] Update or create resource with following computed data: `,
    'silly'
  );
  logger.logComplexData(
    filterQuery,
    `[CreateUpdateResource] Update or create resource matching the following filter query : `,
    'silly'
  );

  // Create or update resource.
  const resource = await Resource.findOneAndUpdate(
    filterQuery,
    {
      ...data,
      $setOnInsert: { _id: newId, cacheDir },
      $inc: { usage },
    },
    { upsert: true, returnNewDocument: true, setDefaultsOnInsert: true, new: true }
  );

  if (resource) {
    logger.logComplexData(resource.toJSON(), `[CreateUpdateResource] Resource updated :`);
    if (resource.usage > 0) {
      // If resource is now in use: ensure cacheDir exists.
      // NOTE: Although it might seem counter-intuitive,
      // deleting the folder first permits images from being rebuilt from cache.
      // This helps with ensuring bookmark images are up-to-date always.
      fileSystem.deleteFolder(resource.cacheDir);
      fileSystem.createFolder(resource.cacheDir);
    } else {
      // If resource is not used anymore: remove it.
      // Remove resource if unused.
      // Currently, the strategy is to delete the resource as soon as no-one uses it anymore.
      logger.debug(`[CreateUpdateResource] Resource is now unused: remove it.`);
      resource.delete();
      logger.logComplexData(
        resource.toJSON(),
        `[CreateUpdateResource] Remove cacheDir for deleted resource:`
      );
      fileSystem.deleteFolder(resource.cacheDir);
      fileSystem.cleanHierarchy(resource.cacheDir);
    }
  }

  return resource;
}

/**
 * Create a bookmark from input data.
 *
 * @param uid (string) the user ID to create the bookmark for.
 * @param input (object) the bookmark input: data to create the bookmark with.
 * @param context
 * @return (Bookmark) the bookmark created
 * @throws err in case of error.
 *  err.statusCode: the REST status code to return for this error
 *  err.message: an error message.
 */
async function createBookmark(uid, input, context = {}) {
  const data = input;

  logger.debug('[CreateBookmark] Perform input checks.');
  logger.logComplexData(data, '[CreateBookmark] Bookmark input data', 'silly');

  // Resolve URL before any further work.
  data.url = fetchService.resolveUrl(data.url);

  // Perform all verification check concurrently for better performance.
  await Promise.all([
    // Validate against JOI schema.
    Bookmark.checkInputValidity(data, context),
    // Validate requested user exists.
    User.checkExists({ _id: uid }, true),
    // Validate bookmark uniqueness for given user.
    Bookmark.checkExists({ uid, url: data.url }, false),
  ]).catch((err) => {
    throw err;
  });

  // Add user data.
  data.uid = uid;

  // Add bookmark tags if none.
  logger.silly('[CreateBookmark] Add empty tags array.');
  if (!data.tags) data.tags = [];

  // Parse the incoming bookmark, and extract infos.
  const $ = await fetchService.fetchOnline(data.url);
  const extractedInfo = {
    url: data.url,
    title: await fetchService.extractTitle($),
    description: await fetchService.extractDescription($),
    favicon: await fetchService.extractFavicon($),
    domain: await fetchService.extractDomain($ || data.url, false),
  };
  logger.logComplexData(extractedInfo, '[FetchService] Extracted info:', 'silly');

  // Concatenate all infos.
  const bookmarkData = { ...extractedInfo, ...data };

  // Create or update the associated resource.
  const resource = await createOrUpdateResource(extractedInfo, 1);
  bookmarkData.rid = resource._id;

  // Build the new bookmark.
  let newBookmark = new Bookmark(bookmarkData);

  // Save the new bookmark.
  logger.silly('[PostBookmark] Save bookmark in storage.');
  newBookmark = await newBookmark.save();
  logger.logComplexData(newBookmark.toObject(), '[PostBookmark] New bookmark data', 'silly');

  return newBookmark;
}

/**
 * Update profile information for the given bookmark.
 *
 * @param uid (string) the user ID to update bookmark for.
 * @param bid (string) the bookmark ID to update
 * @param input (object) the bookmark input: data to update bookmark with.
 * @param context (object) an update context object.
 * @return (Bookmark) the bookmark updated
 * @throws err in case of error.
 *  err.statusCode: the REST status code to return for this error
 *  err.message: an error message.
 */
async function updateBookmark(uid, bid, input, context = {}) {
  const data = input; // Just to please linter :)

  logger.debug(`[UpdateBookmark] Updating bookmark ${bid}`);
  logger.logComplexData(
    data,
    `[UpdateBookmark] Try to update bookmark ${bid} of user ${uid} with following data:`,
    'silly'
  );

  // Resolve URL before any further work.
  if (data.url) {
    data.url = fetchService.resolveUrl(data.url);
  }

  // Perform all verification check concurrently for better performance.
  await Promise.all([
    // Check if user exists
    User.checkExists(uid, true), // @todo: evaluate this: is this possible to have Bookmarks for non existent users ?
    // Validate against JOI schema.
    Bookmark.checkInputValidity(data, context),
    // Validates if bookmark exists.
    Bookmark.checkExists({ _id: bid, uid }, true),
  ]);

  // Set the user.
  logger.silly('[UpdateBookmark] Set user uid.');
  data.uid = uid;

  // Add new bookmark tags.
  logger.silly('[UpdateBookmark] Add empty tags array.');
  if (!data.tags && !context.patch) data.tags = [];

  // Update bookmark.
  const updatedBookmark = await Bookmark.update({ _id: bid, uid }, data);
  if (!updatedBookmark) {
    throw new EntityNotFoundError('Bookmark');
  } else {
    logger.logComplexData(
      updatedBookmark.toObject(),
      '[UpdateBookmark] Bookmark updated with new values:',
      'silly'
    );
  }
  return updatedBookmark;
}

/**
 * Delete given bookmark.
 *
 * @param uid (string) the user ID to delete bookmark for.
 * @param bid (string) the bookmark ID to delete
 * @return (Bookmark) the bookmark deleted
 * @throws err in case of error.
 *  err.statusCode: the REST status code to return for this error
 *  err.message: an error message.
 */
async function deleteBookmark(uid, bid) {
  logger.debug(`[DeleteBookmark] Deleting bookmark ${bid}`);

  // Check if user exists
  await User.checkExists(uid, true);

  // Delete bookmark.
  const deletedBookmark = await Bookmark.delete({ _id: bid, uid });

  if (!deletedBookmark) {
    throw new EntityNotFoundError('Bookmark');
  }

  // Delete the resource (note: no need to wait for it).
  createOrUpdateResource(deletedBookmark.toJSON(), -1);

  return deletedBookmark;
}

//  == REST ==

/**
 * POST /bookmarks/me
 *
 * Store a new bookmark.
 * See validate() in models/bookmark.js for allowed request input.
 */
router.post(
  '/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  async (req, res) => {
    const createdBookmark = await createBookmark(req.user.uid, req.body);
    res.send({ data: createdBookmark });
  }
);

/**
 * POST /bookmarks/user/:uid
 *
 * Store a new bookmark for given user (admin only).
 * See validate() in models/bookmark.js for allowed request input.
 */
router.post(
  '/user/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmarks:all']),
  async (req, res) => {
    const createdBookmark = await createBookmark(req.params.uid, req.body);
    res.send({ data: createdBookmark });
  }
);

/**
 * GET /bookmarks/me
 *
 * Get all bookmarks for requester user..
 */
router.get(
  '/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:read']),
  async (req, res) => {
    const queryParams = req.query.search;
    const searchQuery = searchQueryParser.parse(queryParams, {
      keywords: ['title', 'description', 'url', 'content', 'domain', 'flags', 'tags'],
      tokenize: true,
      alwaysArray: true,
    });

    // Build mongo query
    let queryMongoose = {
      uid: req.user.uid,
    };

    // Add textual full-search search.
    /* if (searchQuery.text) {
      // Mongoose as quite some requirements regarding string searches.
      // see https://docs.mongodb.com/manual/reference/operator/query/text/
      const textSearch = searchQuery.text
        .reduce((acc, item) => {
          return `${acc} ${/\s/.test(item) ? `"${item}"` : item}`;
        }, '')
        .trim();
      queryMongoose = { ...queryMongoose, $text: { $search: textSearch } };
    } */

    // Add textual partial search.
    if (searchQuery.text) {
      const textSearch = searchQuery.text.join('|');
      queryMongoose = {
        ...queryMongoose,
        $or: [
          { title: { $regex: textSearch, $options: 'i' } },
          { description: { $regex: textSearch, $options: 'i' } },
          { url: { $regex: textSearch, $options: 'i' } },
          { domain: { $regex: textSearch, $options: 'i' } },
          { tags: { $regex: textSearch, $options: 'i' } },
        ],
      };
    }

    // Add title search.
    if (searchQuery.title) {
      const titleSearch = searchQuery.title.join('|');
      queryMongoose = { ...queryMongoose, title: { $regex: titleSearch, $options: 'i' } };
    }

    // Add description search.
    if (searchQuery.description) {
      const descriptionSearch = searchQuery.description.join('|');
      queryMongoose = {
        ...queryMongoose,
        description: { $regex: descriptionSearch, $options: 'i' },
      };
    }

    // Add domain search.
    if (searchQuery.domain) {
      const domainSearch = searchQuery.domain.join('|');
      queryMongoose = { ...queryMongoose, domain: { $regex: domainSearch, $options: 'i' } };
    }

    // Add url search.
    if (searchQuery.url) {
      const urlSearch = searchQuery.url.join('|');
      queryMongoose = { ...queryMongoose, url: { $regex: urlSearch, $options: 'i' } };
    }

    // Add content search.
    if (searchQuery.content) {
      const contentSearch = searchQuery.content.join('|');
      queryMongoose = { ...queryMongoose, content: { $regex: contentSearch, $options: 'i' } };
    }

    // Add tags search.
    if (searchQuery.tags) {
      const tagsSearch = searchQuery.tags.join('|');
      queryMongoose = { ...queryMongoose, tags: { $regex: tagsSearch, $options: 'i' } };
    }

    // Add positive flags search.
    if (searchQuery.flags) {
      // Positive flags.
      searchQuery.flags.forEach((flag) => {
        queryMongoose = { ...queryMongoose, [`flags.${flag}`]: true };
      });
    }
    // Add negative flags search.
    if (searchQuery.exclude.flags) {
      searchQuery.exclude.flags.forEach((flag) => {
        queryMongoose = { ...queryMongoose, [`flags.${flag}`]: false };
      });
      delete searchQuery.exclude.flags;
    }

    // Handle exclusion search.
    if (Object.keys(searchQuery.exclude).length) {
      const exclusions = [];
      Object.keys(searchQuery.exclude).forEach((identifier) => {
        searchQuery.exclude[identifier].forEach((term) => {
          exclusions.push({ [identifier]: new RegExp(`.*${term}.*`, 'i') });
        });
      });
      queryMongoose = { ...queryMongoose, $nor: exclusions };
    }

    const bookmarks = await Bookmark.find(queryMongoose).sort({ createdAt: -1 });

    return res.send({ data: bookmarks });
  }
);

/**
 * GET /bookmarks/user/:uid
 *
 * Get all user's bookmarks.
 */
router.get(
  '/user/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookarmks:read']),
  async (req, res) => {
    const [, bookmarks] = await Promise.all([
      await User.checkExists(req.params.uid, true),
      await Bookmark.find({ uid: req.params.uid }).sort({ createdAt: -1 }),
    ]);
    return res.send({ data: bookmarks });
  }
);

/**
 * GET /bookmarks/:bid/me
 *
 * Get's information about a specific bookmark belonging to requester user.
 */
router.get(
  '/:bid([0-9a-fA-F]{24})/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:read']),
  async (req, res) => {
    const bookmark = await Bookmark.checkExists({ _id: req.params.bid, uid: req.user.uid });
    return res.send({ data: bookmark });
  }
);

/**
 * GET /bookmarks/:bid/user/:uid
 *
 * Get's information about the requested bookmark of given user. (only admin)
 */
router.get(
  '/:bid([0-9a-fA-F]{24})/user/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmarks:read']),
  async (req, res) => {
    const [, bookmark] = await Promise.all([
      await User.checkExists(req.params.uid, true),
      await Bookmark.checkExists({ _id: req.params.bid, uid: req.params.uid }),
    ]);
    return res.send({ data: bookmark });
  }
);

/**
 * PUT /bookmarks/:bid/me
 *
 * Replace bookmark information for the given bookmark id (requester bookmark only).
 * This update method only accepts full object update
 */
router.put(
  '/:bid([0-9a-fA-F]{24})/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  async (req, res) => {
    const updatedBookmark = await updateBookmark(req.user.uid, req.params.bid, req.body, {
      put: true,
    });
    return res.send({ data: updatedBookmark });
  }
);

/**
 * PUT bookmarks/:bid/user/:uid
 *
 * Replace bookmark information for the given user.
 * This update method only accepts full object update
 *
 * NOTE: this method is authorized for admin only.
 */
router.put(
  '/:bid([0-9a-fA-F]{24})/user/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmarks:all']),
  async (req, res) => {
    const updatedBookmark = await updateBookmark(req.params.uid, req.params.bid, req.body, {
      put: true,
    });
    return res.send({ data: updatedBookmark });
  }
);

/**
 * PATCH /bookmarks/:bid/me
 *
 * Update bookmark information for the given bookmark id for requester user.
 * This update method accepts partial updates.
 */
router.patch(
  '/:bid([0-9a-fA-F]{24})/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  async (req, res) => {
    const updatedBookmark = await updateBookmark(req.user.uid, req.params.bid, req.body, {
      patch: true,
    });
    return res.send({ data: updatedBookmark });
  }
);

/**
 * PATCH /bookmarks/:bid/user/:uid
 *
 * Update bookmark information for the given user id.
 * This update method accepts partial updates.
 *
 * NOTE: this method is authorized for admin only.
 */
router.patch(
  '/:bid([0-9a-fA-F]{24})/user/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmarks:all']),
  async (req, res) => {
    const updatedBookmark = await updateBookmark(req.params.uid, req.params.bid, req.body, {
      patch: true,
    });
    return res.send({ data: updatedBookmark });
  }
);

/**
 * DELETE /bookmarks/me
 *
 * Delete all bookmarks from the requester.
 */
router.delete(
  '/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  async (req, res) => {
    logger.debug(`[DeleteAllBookmarks] Deleting all bookmark for ${req.user.uid}`);

    // Delete bookmarks.
    const deletedBookmarks = await Bookmark.deleteMultiple({ uid: req.user.uid });

    // Respond as fast as possible.
    res.send({ data: deletedBookmarks.length });

    // Asynchronously delete associated ressource.
    for (let i = 0; i < deletedBookmarks.length; i += 1) {
      // Delete the resource (note: no need to wait for it).
      createOrUpdateResource(deletedBookmarks[i].toJSON(), -1);
    }
  }
);

/**
 * DELETE /bookmarks/:bid/me
 *
 * Delete given bookmark belonging to requester.
 */
router.delete(
  '/:bid([0-9a-fA-F]{24})/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  async (req, res) => {
    const deletedBookmark = await deleteBookmark(req.user.uid, req.params.bid);
    res.send({ data: deletedBookmark });
  }
);

/**
 * DELETE /bookmarks/:bid/user/:uid
 *
 * Delete given bookmark for specified user(admin only).
 */
router.delete(
  '/:bid([0-9a-fA-F]{24})/user/:uid([0-9a-fA-F]{24})',
  auth.isClientAuthentified,
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmarks:all']),
  async (req, res) => {
    const deletedBookmark = await deleteBookmark(req.params.uid, req.params.bid);
    res.send({ data: deletedBookmark });
  }
);

/**
 * POST /bookmarks/import/me
 *
 * Bulk import bookmarks.
 */
router.post(
  '/import/me',
  multer({
    dest: `${fileSystem.getResourcesFolder()}/tmp`,
    fileFilter(req, file, cb) {
      if (
        file.mimetype !== 'text/html' &&
        file.mimetype !== 'application/json' &&
        file.mimetype !== 'application/rss+xml'
      ) {
        return cb(new FileTypeError('This filetype is not allowed.'));
      }
      return cb(null, true);
    },
  }).single('import'),
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  async (req, res) => {
    const contents = fileSystem.readTmpFile(req.file.filename);
    fileParser.parse(contents, (err, { parser, bookmarks }) => {
      if (err) throw new FileParsingError('Impossible to parse given file.');

      const importedItems = flattenTree(
        {
          type: 'root',
          children: bookmarks,
        },
        'children'
      );

      // Create queue and the task for this job.
      const importQueue = new ImportQueue(req.user.uid);
      importQueue.parser = parser;
      importQueue.creationDate = Date.now();
      importQueue.on('queueEmpty', () => {
        removeTask(`import${req.user.uid}`);
      });

      // Only one import per user is possible at a time (to avoid pending task, they are removed after 2h).
      if (addTask(`import${req.user.uid}`, importQueue, 2 * 60 * 60 * 1000)) {
        // Filter the imported items to extract only bookmarks: add then to queue for creation.
        const filteredBookmarks = importedItems.filter((bookmark) => {
          // @todo Give it a second look when working with folders : it might be not that easy to deal with.
          if (bookmark.type === 'bookmark') {
            importQueue.addJob({
              title: bookmark.title,
              url: bookmark.url,
              createdAt: bookmark.createdAt,
              tags: bookmark.tags ? bookmark.tags.slice(0, Bookmark.MAX_TAGS_NUMBER) : [],
            });
            return true;
          }
          return false;
        });

        // Reply as fast as possible.
        res.send({
          parser,
          count: filteredBookmarks.length,
          total: filteredBookmarks.length,
          start: importQueue.creationDate,
        });

        // Let's start the actuall import.
        importQueue.startJobs();
      }
    });
  }
);

/**
 * GET /bookmarks/import/me
 *
 * Checks if an import operation is currently taking place.
 */
router.get(
  '/import/me',
  auth.isUserAuthentified,
  auth.isUserAllowed(['bookmark:all']),
  (req, res) => {
    const importQueue = getTask(`import${req.user.id}`);
    if (importQueue) {
      return res.send({
        parser: importQueue.parser,
        count: importQueue.jobs.length,
        start: importQueue.creationDate,
      });
    }
    return res.send(null);
  }
);

module.exports = {
  router,
  createBookmark,
  createOrUpdateResource,
};
