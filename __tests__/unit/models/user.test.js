/**
 * Run tests over our User model.
 */

const bcrypt = require('bcryptjs');
const config = require('config');
const User = require('../../../models/user');
const Bookmark = require('../../../models/bookmark');
const database = require('../../../services/database');
const { InputValidationError, AuthenticationFailedError } = require('../../../utils/errors');

/**
 * .toObject() should return the User:
 *    - without _id and __v keys.
 *    - _id should be replaced by uid.
 */
describe('user.toObject()', () => {
  it('should return the user without virtuals', async (done) => {
    const payload = {
      email: 'test@test.com',
      password: 'test',
      isAdmin: true,
    };
    const user = new User(payload);

    // Check user has to internal properties.
    await expect(user).toHaveProperty('__v');
    await expect(user).toHaveProperty('_id');

    // Build expected.
    const expected = payload;
    expected.uid = user._id.toString();
    delete expected.password;

    expect(user.toObject()).toMatchObject(expected);

    done();
  });
});

/**
 * .toJson() should return the User:
 *    - without _id and __v keys.
 *    - _id should be replaced by uid.
 *    - password should be removed.
 */
describe('user.toJson()', () => {
  it('should return the user without virtual keys nor password', async (done) => {
    const payload = {
      email: 'test@test.com',
      password: 'test',
      isAdmin: true,
    };
    const user = new User(payload);

    // Check user has to internal properties.
    await expect(user).toHaveProperty('__v');
    await expect(user).toHaveProperty('_id');
    await expect(user).toHaveProperty('password');

    // Build expected.
    const expected = payload;
    expected.uid = user._id.toString();
    delete expected.password;

    await expect(user.toObject()).toMatchObject(expected);
    done();
  });
});

describe('User password should be hashed.', () => {
  it('should return hashedPassword', (done) => {
    const payload = {
      email: 'hash@test.com',
      password: 'test',
    };
    const user = new User(payload);

    // Hash password token.
    expect(user.password).not.toEqual(payload.test);
    done();
  });
});

describe('User.checkInputValidity()', () => {
  it('should a Joi validation is schema validate', async (done) => {
    const schema = {
      email: 'test@test.com',
      password: 'Pa$$w0rd',
    };
    const validation = User.checkInputValidity(schema);
    await expect(validation).toBeTruthy();
    done();
  });

  it('should throw InputValidationError if too much arguments', async (done) => {
    expect.assertions(5);

    const schema = {
      email: 'test@test.com',
      password: 'Pa$$w0rd',
      isAdmin: true,
    };
    const validation = User.checkInputValidity(schema);
    await expect(validation).rejects.toThrow(InputValidationError);

    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('isAdmin', '"isAdmin" is not allowed');
    });

    done();
  });

  it('should throw InputValidationError if password is missing', async (done) => {
    expect.assertions(5);
    const schema = {
      email: 'test@test.com',
    };

    const validation = User.checkInputValidity(schema);
    await expect(validation).rejects.toThrow(InputValidationError);

    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('password', '"password" is required');
    });

    done();
  });

  it('should throw InputValidationError if password is too short', async (done) => {
    expect.assertions(5);
    const schema = {
      email: 'test@test.com',
      password: 'test',
    };

    const validation = User.checkInputValidity(schema);
    await expect(validation).rejects.toThrow(InputValidationError);

    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty(
        'password',
        '"password" length must be at least 8 characters long'
      );
    });

    done();
  });

  it('should validates weak password in login mode', async (done) => {
    expect.assertions(1);
    const schema = {
      email: 'test@test.com',
      password: 'test',
    };

    const validation = User.checkInputValidity(schema, { login: true });
    await expect(validation).resolves.toBeTruthy();

    done();
  });

  it('should allow password to be missing in patch mode', async (done) => {
    expect.assertions(1);
    const schema = {
      email: 'test@test.com',
    };

    const validation = User.checkInputValidity(schema, { patch: true });
    await expect(validation).resolves.toBeTruthy();

    done();
  });

  it('should throw InputValidationError if email is missing', async (done) => {
    expect.assertions(5);
    const schema = {
      password: 'pa$$word',
    };
    const validation = User.checkInputValidity(schema);
    await expect(validation).rejects.toThrow(InputValidationError);

    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('email', '"email" is required');
    });

    done();
  });

  it('should validate a missing email in patch mode', async (done) => {
    const schema = {
      password: 'pa$$word',
    };
    const validation = User.checkInputValidity(schema, { patch: true });
    await expect(validation).resolves.toBeTruthy();
    done();
  });

  it('should throw InputValidationError if arguments is malformed', async (done) => {
    expect.assertions(5);
    const schema = {
      email: 'test',
      password: '123',
    };
    const validation = User.checkInputValidity(schema);
    await expect(validation).rejects.toThrow(InputValidationError);

    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty(
        'email',
        '"email" with value "test" fails to match the email format pattern'
      );
    });

    done();
  });

  it('should validate missing mandatory arguments (ie partial validation) in update mode', async (done) => {
    expect.assertions(2);
    const schemaPassword = {
      password: 'Pa$$wr0rd',
    };
    const validationPassword = User.checkInputValidity(schemaPassword, { patch: true });
    await expect(validationPassword).toBeTruthy();
    const schemaEmail = {
      email: 'test@test.com',
    };
    const validationEmail = User.checkInputValidity(schemaEmail, { patch: true });
    await expect(validationEmail).toBeTruthy();
    done();
  });
});

describe('User.checkPasswordMatch()', () => {
  it('should validate if password matches', async (done) => {
    const password = 'Pa$$w0rd!';
    const user = {
      email: 'admin@test.com',
      password: bcrypt.hashSync(password, 10),
    };
    const validation = User.checkPasswordMatch(user, password);
    await expect(validation).toBeTruthy();
    done();
  });

  it('should throw AuthenticationFailedError if password does not match', async (done) => {
    const password = 'wrong';
    const user = {
      password: '$2a$10$f6659p1L.XrrCNQvmWak.OKm30ovmQOu6iGUfvhXs3jX.eE.wrqti',
    };

    const validation = User.checkPasswordMatch(user, password);
    await expect(validation).rejects.toThrow(AuthenticationFailedError);
    done();
  });

  it('should throw AuthenticationFailedError if comparison password is missing', async (done) => {
    const user = {
      password: '$2a$10$f6659p1L.XrrCNQvmWak.OKm30ovmQOu6iGUfvhXs3jX.eE.wrqti',
    };
    const validation = User.checkPasswordMatch(user);
    await expect(validation).rejects.toThrow(AuthenticationFailedError);
    done();
  });

  it('should throw AuthenticationFailedError if password is missing', async (done) => {
    const password = 'wrong';
    const user = {
      email: 'dom.clause@gmail.com',
    };
    const validation = User.checkPasswordMatch(user, password);
    await expect(validation).rejects.toThrow(AuthenticationFailedError);
    done();
  });
});

describe('User.checkPasswordStrength()', () => {
  it('should validate if password strength is good enough', (done) => {
    const password = 'Pa$$w0rdIsSoSt0nG!';
    const validation = User.checkPasswordStrength(password);
    expect(validation).not.toBeNaN(); // Expect number.
    done();
  });

  it('should throw InputValidationError is password is blacklisted', async (done) => {
    expect.assertions(5);
    const password = 'admin';
    const validation = User.checkPasswordStrength(password);
    await expect(validation).rejects.toThrow(InputValidationError);

    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('password', 'Password is blacklisted.');
    });

    done();
  });

  it('should throw InputValidationError is password is too weak', async (done) => {
    expect.assertions(5);
    const password = 'woomind';
    const requiredEntropy = config.get('security.password_entropy') || 45;
    const validation = User.checkPasswordStrength(password);

    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty(
        'password',
        `Password is too weak. Current entropy is 33, requires above ${requiredEntropy}`
      );
    });

    done();
  });

  it('should throw InputValidationError is password missing.', async (done) => {
    expect.assertions(5);
    const validation = User.checkPasswordStrength();

    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('password', '"Password" is required.');
    });
    done();
  });
});

describe('User.remove()', () => {
  let user;
  let bookmark;

  beforeAll(async (done) => {
    await database.start();
    done();
  });
  afterAll(async (done) => {
    User.deleteMany({});
    Bookmark.deleteMany({});
    await database.stop();
    done();
  });

  beforeEach(async (done) => {
    user = await new User({
      email: 'bookmarkuser@test.com',
      password: 'Pa$$w0rd!',
    }).save();

    bookmark = await new Bookmark({
      uid: user._id,
      url: 'https://woomind.com/bookmark',
      tags: ['tag1', 'tag2', 'tag3', 'tag4', 'tag5'],
      flags: 13,
    }).save();
    done();
  });

  it('should remove user associated data', async (done) => {
    expect.assertions(2);

    // Remove user
    await user.remove();

    // Validate user is deleted.
    const deletedUser = await User.findBy(user._id);
    expect(deletedUser).toBeFalsy();

    // Validate associated bookmark is deleted.
    const deletedBookmark = await Bookmark.findBy(bookmark._id);
    expect(deletedBookmark).toBeFalsy();
    done();
  });
});
