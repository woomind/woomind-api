/**
 * Run tests over our Bookmark model.
 */
const Bookmark = require('../../../models/bookmark');
const { InputValidationError } = require('../../../utils/errors');

/**
 * .toObject() should return the Bookmark:
 *    - without _id and __v keys.
 *    - _id should be replaced by bid.
 *    - uid should be removed.
 */
describe('bookmark.toObject()', () => {
  it('should return the bookmark without virtual keys', async (done) => {
    const payload = {
      url: 'http://woomind.com/to_object',
      tags: ['tag1', 'tag2', 'tag3', 'tag4', 'tag5'],
      flags: 13,
    };
    const bookmark = new Bookmark(payload);

    // Check bookmark has to internal properties.
    await expect(bookmark).toHaveProperty('__v');
    await expect(bookmark).toHaveProperty('_id');

    // Build expected.
    const expected = payload;
    expected.bid = bookmark._id.toString();
    delete expected.uid;
    expected.flags = {
      read: true,
      favorite: false,
      archive: true,
      read_later: true,
    };
    expect(bookmark.toObject()).toMatchObject(expected);
    done();
  });
});

/**
 * .toJSON() should return the Bookmark:
 *    - without _id and __v keys.
 *    - _id should be replaced by bid.
 *    - uid should be removed.
 */
describe('bookmark.toJSON()', () => {
  it('should return the bookmark without virtual keys', async (done) => {
    const payload = {
      url: 'http://woomind.com/to_object',
      tags: ['tag1', 'tag2', 'tag3', 'tag4', 'tag5'],
      flags: 13,
    };
    const bookmark = new Bookmark(payload);

    // Check bookmark has to internal properties.
    await expect(bookmark).toHaveProperty('__v');
    await expect(bookmark).toHaveProperty('_id');

    // Build expected.
    const expected = payload;
    expected.bid = bookmark._id.toString();
    expected.flags = {
      read: true,
      favorite: false,
      archive: true,
      read_later: true,
    };
    delete expected.uid;
    expect(bookmark.toJSON()).toMatchObject(expected);
    done();
  });
});

/**
 * .checkInputValidity(payload) should validate a bookmark payload.
 */
describe('bookmark.checkInputValidity(payload)', () => {
  it('should validate if payload with minimal data is good.', async (done) => {
    const payload = {
      url: 'https://woomind.com/bookmark',
    };
    const validation = await Bookmark.checkInputValidity(payload);
    expect(validation).toBe(true);
    done();
  });
  it('should validate if payload with optional data is good.', async (done) => {
    const payload = {
      url: 'https://woomind.com/bookmark',
      tags: ['tag1', 'tag2', 'tag3', 'tag4', 'tag5'],
      flags: {
        read: true,
        archive: false,
        favorite: true,
        read_later: true,
      },
    };
    const validation = await Bookmark.checkInputValidity(payload);
    expect(validation).toBe(true);
    done();
  });
  it('should throw an error if tags is not an array.', async (done) => {
    expect.assertions(5);
    const payload = {
      url: 'https://woomind.com/bookmark',
      tags: {},
      flags: {},
    };
    const validation = Bookmark.checkInputValidity(payload);
    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('tags', '"tags" must be an array');
    });
    done();
  });
  it('should throw an error if tags values are no string.', async (done) => {
    expect.assertions(5);
    const payload = {
      url: 'https://woomind.com/bookmark',
      tags: [1, 2, 3],
      flags: {},
    };
    const validation = Bookmark.checkInputValidity(payload);
    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('tags', '"2" must be a string');
    });
    done();
  });
  it('should throw an error if tags has too many values.', async (done) => {
    expect.assertions(5);
    const tags = [];
    for (let i = 0; i <= Bookmark.MAX_TAGS_NUMBER + 10; i += 1) {
      tags.push(`tag${i}`);
    }
    const payload = {
      url: 'https://woomind.com/bookmark',
      tags,
      flags: {},
    };
    const validation = Bookmark.checkInputValidity(payload);
    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty(
        'tags',
        `"tags" must contain less than or equal to ${Bookmark.MAX_TAGS_NUMBER} items`
      );
    });
    done();
  });
  it('should throw an error if flags is not an object.', async (done) => {
    expect.assertions(5);
    const payload = {
      url: 'https://woomind.com/bookmark',
      tags: [],
      flags: 0,
    };
    const validation = Bookmark.checkInputValidity(payload);
    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('flags', '"flags" must be an object');
    });
    done();
  });
  it('should throw an error if flags has a wrong property.', async (done) => {
    expect.assertions(5);
    const payload = {
      url: 'https://woomind.com/bookmark',
      tags: [],
      flags: {
        wrong: true,
      },
    };
    const validation = Bookmark.checkInputValidity(payload);
    await expect(validation).rejects.toThrow(InputValidationError);
    await validation.catch(async (err) => {
      await expect(err).toHaveProperty('status', 400);
      await expect(err).toHaveProperty('message', 'One or more fields raised validation errors');
      await expect(err).toHaveProperty('fields');
      await expect(err.fields).toHaveProperty('flags', '"wrong" is not allowed');
    });
    done();
  });
});
