describe('Application should crash if database connection fails.', () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules(); // this is important
    process.env = { ...OLD_ENV };
  });

  afterEach(() => {
    process.env = OLD_ENV;
  });

  test("Should throw an exception if database can't be accessed", async () => {
    expect.assertions(1);
    try {
      process.env.WOOMIND_API_DB_URL = 'wrong';
      const Database = require('../../../services/database'); // eslint-disable-line global-require
      await Database.start();
    } catch (e) {
      expect(e.message).toEqual('Connection to database: FAILED');
    }
  }, 45000);

  test('Should skip authentication if user or password is not provided', async () => {
    expect.assertions(1);
    process.env.WOOMIND_API_DB_USER = 'john';
    const Database = require('../../../services/database'); // eslint-disable-line global-require
    const db = await Database.start();
    expect(db).toBeTruthy();
  });
});
