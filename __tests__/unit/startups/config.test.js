describe('Application should crash if configs are missing.', () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules(); // this is important
    process.env = { ...OLD_ENV };
    process.env.NODE_ENV = 'foobar';
    process.env.WOOMIND_API_LOG_LEVEL = 'none';
    process.env.WOOMIND_API_HTTPS_PRIVATE_KEY_PATH = 'defined';
    process.env.WOOMIND_API_HTTPS_CERTIFICATE_PATH = 'defined';
    process.env.WOOMIND_API_DB_URL = 'defined';
    process.env.WOOMIND_API_DB_PORT = 'defined';
    process.env.WOOMIND_API_DB_NAME = 'defined';
    process.env.WOOMIND_API_DB_USER = 'defined';
    process.env.WOOMIND_API_DB_PASSWORD = 'defined';
  });

  afterEach(() => {
    process.env = OLD_ENV;
  });

  test('Should not throw an exception if no certfiles are configured in HTTP mode.', async (done) => {
    expect.assertions(0);
    try {
      process.env.WOOMIND_API_ENABLE_HTTPS = false;
      require('../../../startup/app'); // eslint-disable-line global-require
    } catch (e) {
      expect(e).not.toBeNull();
    }
    done();
  });

  test('Should throw an exception if HTTPS and private key config is empty', async (done) => {
    expect.assertions(1);
    try {
      process.env.WOOMIND_API_HTTPS_PRIVATE_KEY_PATH = '';
      require('../../../startup/app'); // eslint-disable-line global-require
    } catch (e) {
      expect(e.message).toEqual(
        'FATAL ERROR: certfiles are not defined. Please consult README within certfiles folder.'
      );
    }
    done();
  });

  test('Should throw an exception if HTTPS and certificate config is empty', async (done) => {
    expect.assertions(1);
    try {
      process.env.WOOMIND_API_HTTPS_CERTIFICATE_PATH = '';
      require('../../../startup/app'); // eslint-disable-line global-require
    } catch (e) {
      expect(e.message).toEqual(
        'FATAL ERROR: certfiles are not defined. Please consult README within certfiles folder.'
      );
    }
    done();
  });

  test('Should throw an exception if database config is missing', async (done) => {
    expect.assertions(1);
    try {
      delete process.env.WOOMIND_API_DB_URL;
      delete process.env.WOOMIND_API_DB_PORT;
      delete process.env.WOOMIND_API_DB_NAME;
      delete process.env.WOOMIND_API_DB_USER;
      delete process.env.WOOMIND_API_DB_PASSWORD;
      require('../../../startup/app'); // eslint-disable-line global-require
    } catch (e) {
      expect(e.message).toEqual('FATAL ERROR: database connection info are not defined');
    }
    done();
  });
});
