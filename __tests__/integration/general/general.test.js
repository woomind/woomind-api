const request = require('supertest');
const User = require('../../../models/user');
const { app, db } = require('../../../startup/app');

describe('Undefined route should return status 404', () => {
  it('should return status 404 if GET route is undefined', async () => {
    const executeRequest = () => request(app).get('/wrong').send();
    const result = await executeRequest();
    expect(result.status).toBe(404);
    expect(result.text).toBe('Route not found');
  });
  it('should return status 404 if POST route is undefined', async () => {
    const executeRequest = () => request(app).post('/wrong').send();
    const result = await executeRequest();
    expect(result.status).toBe(404);
    expect(result.text).toBe('Route not found');
  });
  it('should return status 404 if PUT route is undefined', async () => {
    const executeRequest = () => request(app).put('/wrong').send();
    const result = await executeRequest();
    expect(result.status).toBe(404);
    expect(result.text).toBe('Route not found');
  });
  it('should return status 404 if PATCH route is undefined', async () => {
    const executeRequest = () => request(app).patch('/wrong').send();
    const result = await executeRequest();
    expect(result.status).toBe(404);
    expect(result.text).toBe('Route not found');
  });
  it('should return status 404 if DELETE route is undefined', async () => {
    const executeRequest = () => request(app).delete('/wrong').send();
    const result = await executeRequest();
    expect(result.status).toBe(404);
    expect(result.text).toBe('Route not found');
  });
});

describe('Un-prefixed API should use latest version', () => {
  let userTest;
  const executeRequest = () => request(app).get('/users/me').send();
  const executeRequestV2 = () => request(app).get('/v2/users/me').send();

  beforeEach(async (done) => {
    // Create a standard user.
    userTest = new User({
      email: 'version@test.com',
      password: 'Pa$$w0rd!',
    });
    userTest = await userTest.save();
    done();
  });
  afterEach(async (done) => {
    await User.deleteMany({});
    done();
  });

  it('should get redirection GET /users -> GET /v2/users', async () => {
    // Validate response un-prefixed API.
    const result = await executeRequest();
    expect(result.status).not.toBe(200);

    // Validate response V2 API.
    const resultV2 = await executeRequestV2();
    expect(resultV2.status).not.toBe(200);

    // Both should be the same.
    expect(result.body).toMatchObject(resultV2.body);
  });
});

describe('Application should support unexpected errors.', () => {
  const executeRequest = () =>
    request(app).post('/users/register').send({
      email: 'version@test.com',
      password: 'Pa$$w0rd!',
    });

  beforeEach(async (done) => {
    await db.stop();
    done();
  });

  it('should return error when database crashes.', async () => {
    User.checkExists = jest.fn(() => {
      throw new Error();
    });

    const result = await executeRequest();
    expect(result.status).not.toBe(200);
    expect(result.body).toBeInstanceOf(Object);
  });
});
