const request = require('supertest');
const User = require('../../../models/user');
const { app } = require('../../../startup/app');

describe('Endpoints should be as secured as possible', () => {
  const executeRequest = () => request(app).get('/').send();

  beforeEach(async (done) => {
    const userTest = new User({
      email: 'security@test.com',
      password: 'Pa$$w0rd',
    });
    await userTest.save();
    done();
  });
  afterEach(async (done) => {
    await User.deleteMany({});
    done();
  });

  it('should not send X-Powered-By header', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['x-powered-by']).toBeUndefined();
  });
  it('should send X-Download-Options header to noopen', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['x-download-options']).toBe('noopen');
  });
  it('should send X-Content-Type-Options header to nosniff', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['x-content-type-options']).toBe('nosniff');
  });
  it('should send X-XSS-Protection header to 1; mode=block', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['x-xss-protection']).toBe('1; mode=block');
  });
  it('should send X-Frame-Options header to SAMEORIGIN', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['x-frame-options']).toBe('SAMEORIGIN');
  });
  it('should send X-DNS-Prefetch-Control to off', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['x-dns-prefetch-control']).toBe('off');
  });
  it('should send Strict-Transport-Security to max-age=15552000; includeSubDomains', async () => {
    const res = await executeRequest();
    expect(res.status).toBe(200);
    expect(res.headers['strict-transport-security']).toBe('max-age=15552000; includeSubDomains');
  });
});
