const request = require('supertest');
const User = require('../../../models/user');
const { app } = require('../../../startup/app');

describe('Endpoints should be able to run concurrent requests.', () => {
  const calls = 100;
  let userTest;

  beforeAll(async (done) => {
    jest.setTimeout(15000);
    done();
  });
  afterAll(async (done) => {
    jest.setTimeout(5000);
    done();
  });

  beforeEach(async (done) => {
    userTest = new User({
      email: 'performance@test.com',
      password: 'Pa$$w0rd',
    });
    await userTest.save();
    done();
  });
  afterEach(async (done) => {
    await userTest.delete();
    done();
  });

  it(`should support ${calls} concurrent GET requests`, async (done) => {
    expect.assertions(calls);

    const asyncCall = async () => {
      // await the actual stuff
      const response = await request(app).get('/').send();
      // check the result of our stuff
      expect(response.status).toBe(200);
      return response;
    };

    const promises = [];
    for (let i = 0; i < calls; i += 1) {
      promises.push(asyncCall());
    }
    await Promise.all(promises);
    done();
  });
});
