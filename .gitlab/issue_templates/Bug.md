# Sanity check

* [ ] Are you sure your repositories are up-to date?
* [ ] Have you tested with multiple browsers?
* [ ] Have you tested without browser extensions? (e.g. incognito mode)

# Describe what is happening in details

# How can we reproduce?

* `woomind-api` commit ID or branch: 
* `woomind-web` commit ID or branch: 