/**
 * Entry point for WooMind API application.
 */

// Imports
const fs = require('fs');
const config = require('config');
const http = require('http');
const https = require('https');
const { app } = require('./startup/app');
const logger = require('./services/logger');
const AccessToken = require('./models/access_token');
const { privateKey, certificate } = require('./utils/security');

// From time to time we need to clean up any expired tokens in the database.
function clearTokens() {
  AccessToken.removeExpired()
    .then(logger.info('Cleanup of expired tokens done.'))
    .catch((err) => logger.error('Error trying to remove expired tokens:', err.stack));
}
clearTokens();
setInterval(clearTokens, config.get('security.access_token_cleanup'));

// Run over http.
if (config.get('http.enable')) {
  const httpPort = config.get('http.port') || 3000;
  const httpServer = http.createServer(app);
  httpServer.listen(httpPort, () => {
    logger.info(`Listening http protocol on port ${httpPort}...`);
  });
}

// Run over https.
if (
  config.get('https.enable') &&
  config.has('https.privateKey') &&
  config.has('https.privateKey')
) {
  const httpsPort = config.get('https.port') || 3443;
  const credentials = { key: privateKey, cert: certificate };
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(httpsPort, () => {
    logger.info(`Listening https protocol on port ${httpsPort}...`);
  });
}

module.exports = app;
