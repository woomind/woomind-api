/**
 * Database service: helps to start()/stop() database.
 */

// Imports.
const mongoose = require('mongoose');
const config = require('config');
const logger = require('./logger');

class Database {
  /**
   * Starts the database.
   * @returns {Connection}
   */
  static async start() {
    const dbUrl = config.get('database.url');
    const dbPort = config.get('database.port');
    const dbName = config.get('database.name');

    // Build connection string.
    let dbInfo = `${dbUrl}:${dbPort}/${dbName}`;

    // Use authentication if provided.
    if (config.has('database.user') && config.has('database.password')) {
      dbInfo = `${config.get('database.user')}:${config.get('database.password')}@${dbInfo}`;
    }

    // Attempt connection.
    logger.debug(`Attempt mongo connection at: ${dbInfo}`);
    const connection = await mongoose
      .connect(`mongodb://${dbInfo}`, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .catch((err) => {
        logger.error('Connection to database: FAILED', err);
        throw new Error('Connection to database: FAILED');
      });
    logger.debug(`Database connection successful`);
    return connection;
  }

  /**
   * Close database connection.
   * @return Connection
   */
  static async stop() {
    await mongoose.connection.close().then(() => {
      logger.debug(`Database disconnect successful`);
    });
  }
}

module.exports = Database;
