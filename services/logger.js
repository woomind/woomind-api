/**
 * Startup for winston module:
 * Defines transports for handling logs.
 */

// Imports.
const config = require('config');
const winston = require('winston');

// Define logger options.
const logFormat = winston.format.combine(
  // Colorize full line.
  winston.format.colorize({ all: true }),
  // Display timestamp.
  winston.format.timestamp(),
  // Align result line section.
  winston.format.align(),
  // Custom line format.
  winston.format.printf((info) => {
    const { timestamp, level, message, ...args } = info;
    const ts = timestamp.slice(0, 19).replace('T', ' ');
    const data =
      message instanceof Object || message instanceof Array
        ? JSON.stringify(message, null, '\t')
        : message;
    return `${ts} [${level}]: ${data} ${
      Object.keys(args).length ? JSON.stringify(args, null, '\t') : ''
    }`;
  })
);

// Define the custom settings for each transport (file, console)
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.simple(),
  prettyPrint: true,
});

// Set requested logLevel
const logLevel = config.get('logs.level') || 'info';
logger.logLevel = logLevel;

// Overrides default colors per log level.
winston.addColors({
  error: 'red',
  warn: 'yellow',
  info: 'green',
  debug: 'magenta',
  silly: 'cyan',
});

// If we are not in test, log in files.
const logPath = config.get('logs.path') || './logs';
if (process.env.NODE_ENV !== 'test') {
  // - Write all logs error (and below) to `error.log`.
  logger.add(new winston.transports.File({ filename: `${logPath}/error.log`, level: 'error' }));
  // - Write to all logs with level `info` and below to `combined.log`
  logger.add(new winston.transports.File({ filename: `${logPath}/combined.log` }));
} else {
  logger.add(new winston.transports.File({ filename: `${logPath}/tests.log` }));
}

// If we are not in production, also log everything in the console.
if (process.env.NODE_ENV !== 'production' && logLevel !== 'none') {
  logger.add(
    new winston.transports.Console({
      level: logLevel,
      format: logFormat,
    })
  );
}

/**
 * This method is used to debug complex objects / arrays into a nice looking display.
 *
 * @param data An object to display
 * @param debugHeader string Optionally the name of debugged variable.
 * @param level  The log level to use.
 */
logger.logComplexData = (data, debugHeader = '', level = 'debug') => {
  if (debugHeader) logger.debug(debugHeader);
  if (data instanceof Object) {
    Object.keys(data).forEach((key) => {
      logger.log(level, `\t${key}  =>  ${JSON.stringify(data[key])}`);
    });
  } else {
    logger.log(level, JSON.stringify(data));
  }
};

logger.debug(`Log level now set to: ${logLevel}`);
module.exports = logger;
