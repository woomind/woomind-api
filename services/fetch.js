const cheerio = require('cheerio');
const got = require('got');
const urlUtil = require('url');
const charsetConverter = require('iconv-lite');
const headerCharsetReader = require('charset');
const charsetDetector = require('jschardet');
const imageSize = require('probe-image-size');
const fs = require('fs');
const HtmlEntityDecoder = require('html-entities').AllHtmlEntities;
const sharp = require('sharp');
const smartCrop = require('smartcrop-sharp');
const logger = require('./logger');

const htmlEntityDecoder = new HtmlEntityDecoder();

/**
 * Helper method: HTML decode, unicode convert and entity decode input.
 *
 * @param input
 *  The content to decode
 * @returns string
 *  The decoded content
 */
const htmlDecode = (input) => htmlEntityDecoder.decode(input);

/**
 * Fetch the given URL and run it inside cheerio parser.
 * @param url
 * @returns a cheerio's enabled parsed content
 */
const fetchOnline = async (url) => {
  let response;

  // Call resource online.
  logger.debug(`[InitCheerioParser] Fetching url: ${url}`);
  try {
    response = await got(url, { responseType: 'buffer', timeout: 5000 });
  } catch (err) {
    logger.error(`[InitCheerioParser] Could not be initialized from ${url} because ${err.message}`);
    logger.logComplexData(err.gotOptions, 'fails with following options:', 'error');
    ({ response } = err);
  }
  if (!response) {
    return null;
  }
  // Detect response encoding
  let charset = headerCharsetReader(response.headers, response.body);
  charset = charset || charsetDetector.detect(response.body).encoding.toLowerCase();

  // Convert charset encoding.
  let { body } = response;
  try {
    body = charsetConverter.decode(body, charset);
  } catch (e) {
    /* */
  }

  // Run through cheerio.
  const $ = cheerio.load(body);
  // Extend cheerio's object.
  $.response = response;

  return $;
};

/**
 * Extracts the domain from an HTML content.
 *
 * @param $
 * @param useFullBase
 *  FALSE (default): the domain only (exemple.fr) will be returned
 *  TRUE: the full base domain to be considered for relative resources will be returned.
 *  This is computed from <base> tag if available + the real resolved URL.
 * @returns string
 *  The domain
 */
const extractDomain = ($, useFullBase = false) => {
  const url = $ && $.response && $.response.url ? $.response.url : $;
  if (!url) return '';

  if (useFullBase) {
    // Retrieve <base> tag value.
    const base = $('base[href!=""]').attr('href');
    // Resolve <base> URL (can be relative or absolute).
    if (base) return urlUtil.resolve(url, base);
  }

  // Parse the given URL and extract domain (with or without protocol)
  const { protocol, host, hostname } = urlUtil.parse(url);
  let domain = useFullBase ? `${protocol}//${host}` : host.replace('www.', '');

  // According to documentation, host can be empty in favor of hostname.
  if (!host) {
    domain += useFullBase ? hostname : hostname.replace('www.', '');
  }
  return domain;
};

/**
 * Extracts the best title from an HTML content.
 *
 * @param $
 * @returns string
 *  The title
 */
const extractTitle = ($) => {
  if (!$) return '';

  let title = $(
    'meta[property="og:title" i][content!=""], meta[name="twitter:title" i][content!=""], meta[name="title" i][content!=""]'
  )
    .last()
    .attr('content');
  title = title || $('title').first().text() || $.response.url;
  return htmlDecode(title);
};

/**
 * Extracts the best favicon from an HTML content.
 *
 * @param $
 * @returns string
 *  The favicon URL.
 */
const extractFavicon = async ($) => {
  if (!$) return '';

  let favicon = $(
    'link[rel="fluid-icon" i][href!=""], link[rel="icon" i][href!=""], link[rel="shortcut icon" i][href!=""]'
  )
    .last()
    .attr('href');
  if (favicon) {
    favicon = urlUtil.resolve(extractDomain($, true), favicon);
    await got(favicon).catch(() => {
      favicon = '';
    });
  }

  if (!favicon) {
    favicon = urlUtil.resolve(extractDomain($, true), 'favicon.ico');
    await got(favicon).catch(() => {
      favicon = '';
    });
  }

  return favicon;
};

/**
 * Extracts the best description from an HTML content.
 *
 * @param $
 * @returns string
 *  The description
 */
const extractDescription = async ($) => {
  if (!$) return '';

  let description = $(
    'meta[property="og:description" i][content!=""], meta[name="twitter:description" i][content!=""], meta[http-equiv="description" i][content!=""], meta[name="description" i][content!=""]'
  )
    .last()
    .attr('content');
  description = htmlDecode(description);
  return description.length ? `${description.substring(0, 200).trim()}...` : '';
};

/**
 * Find the most appropiate image URL inside a 'cheerio' parsed website, according to request dimensions.
 * @param $
 * @param expectedWidth
 * @param expectedHeight
 * @returns {Promise<null>}
 */
const findAppropriateCoverImage = async ($, expectedWidth, expectedHeight) => {
  const domainUrl = extractDomain($, true);
  let coverImage = null;

  // Find og:image or twitter:image.
  let coverImages = $(
    'meta[property="og:image" i][content!=""], meta[name="twitter:image" i][content!=""]'
  );

  // If none, find image from article.
  if (!coverImages.length) {
    coverImages = $('article img[src!=""]:not([src^="data"]):not([src$=".gif"])').slice(0, 10);
  }

  // If none find any image inside body.
  if (!coverImages.length) {
    coverImages = $('body img[src!=""]:not([src^="data"]):not([src$=".gif"])').slice(0, 10);
  }

  if (coverImages.length) {
    logger.debug(`[ExtractCoverImage] ${coverImages.length} potential images found.`);

    // -- FIND IMAGES WIDTH
    // Map array to promises.
    let promises = coverImages
      .map((i, elem) => {
        coverImage = urlUtil.resolve(domainUrl, $(elem).attr('src') || $(elem).attr('content'));
        logger.debug(`[ExtractCoverImage] Extracting size of: ${coverImage}`);
        return imageSize(coverImage, { timeout: 5000 });
      })
      .get();
    promises = Array.isArray(promises) ? promises : [promises];

    // Wait until all promises are resolved
    const sizes = await Promise.all(promises.map((p) => p.catch(() => undefined)));
    // Get the coverImage with the highest width.
    logger.debug(`[ExtractCoverImage] Determining best suitable size image.`);
    coverImage = sizes.reduce(
      (acc, elem) => {
        if (!elem) {
          return acc;
        }
        if (
          (elem.width >= expectedWidth || elem.height >= expectedHeight) &&
          elem.width > acc.width &&
          elem.height > acc.height
        ) {
          return elem;
        }
        return acc;
      },
      { height: 1, width: 1, url: coverImage }
    );
  }

  // Return the coverImage.
  coverImage = coverImage ? coverImage.url : null;
  logger.debug(`[ExtractCoverImage] Best image found: ${coverImage}.`);
  return coverImage;
};

/**
 * Given a URL, return a stored image that match the requested dimensions.
 * @param url
 * @param expectedWidth
 * @param expectedHeight
 * @returns {Promise<*>}
 */
const findAppropriateCoverImageFromUrl = async (url, expectedWidth, expectedHeight) => {
  logger.debug(`[ExtractCoverImage] Extracting image from: ${url}`);
  const $ = await fetchOnline(url);
  if (!$) {
    return null;
  }
  return findAppropriateCoverImage($, expectedWidth, expectedHeight);
};

/**
 * Applies a smart croping to an image.
 * @param source
 *  The source image (can be a URI)
 * @param dest
 *  The destination image path (must be different from source, and directory path must exist)
 * @param width
 *  The requested image width
 * @param height
 *  The requested image height
 * @returns {Promise<void>}
 */
const applySmartCrop = async (source, dest, width, height) => {
  const { topCrop } = await smartCrop.crop(source, {
    width,
    height,
    skinBias: 0.1,
  });
  logger.logComplexData(
    topCrop,
    '[applySmartCrop] Detection found best suitable crop at:',
    'silly'
  );
  await sharp(source)
    .extract({ width: topCrop.width, height: topCrop.height, left: topCrop.x, top: topCrop.y })
    .flatten({ background: '#ffffff' })
    .resize(width, height)
    .jpeg()
    .toFile(dest);
};

/**
 * Extract a standardized resource URL for the given bookmark.
 * NOTE: currently only supports returning the base URL, but could be expanded to further process.
 * @param $
 * @returns string the URL
 */
const resolveUrl = ($) => {
  const url = $ && $.response && $.response.url ? $.response.url : $;
  if (!url) return '';

  const forbiddenParams = [
    'utm_medium',
    'utm_term',
    'utm_content',
    'utm_campaign',
    'dclid',
    'gclsrc',
    'os_ehash',
    'access_token',
    'utm_source',
    'hConversionEventId',
  ];

  let rtn = url.split('?')[0];
  let param;
  let paramsArr = [];

  const queryString = url.indexOf('?') !== -1 ? url.split('?')[1] : '';
  if (queryString !== '') {
    paramsArr = queryString.split('&');
    for (let i = paramsArr.length - 1; i >= 0; i -= 1) {
      [param] = paramsArr[i].split('=');
      if (forbiddenParams.indexOf(param) > -1) {
        paramsArr.splice(i, 1);
      }
    }
    rtn = `${rtn}${paramsArr.length ? '?' : ''}${paramsArr.join('&')}`;
  }
  logger.debug(`[ResolveUrl] Input URL (${url}) resolved to ${rtn}`);
  return rtn;
};

const extractCoverImageForBookmark = async (bookmark, expectedWidth = 70, expectedHeight = 70) => {
  // Build destination path.
  const filePath = `${bookmark.rid.cacheDir}/${expectedWidth}_${expectedHeight}.jpg`;

  // Check if file already exists at destination.
  if (!fs.existsSync(filePath)) {
    // Find the best suitable coverImage at the given URL.
    const coverImageUrl = await findAppropriateCoverImageFromUrl(
      bookmark.url,
      expectedWidth,
      expectedHeight
    );
    if (coverImageUrl) {
      logger.debug(`[ExtractCoverImage] Best suitable coverImage found at: ${coverImageUrl}`);
      // Load the coverImage.
      const response = await got(coverImageUrl, { responseType: 'buffer' });
      if (response) {
        // Run through smart crop.
        logger.debug(
          `[ExtractCoverImage] Run coverImage through smartcrop, saving to: ${filePath}`
        );
        await applySmartCrop(
          response.body,
          filePath,
          Number(expectedWidth),
          Number(expectedHeight)
        );
      }
    } else {
      logger.debug(`[ExtractCoverImage] No suitable coverImage found for ${bookmark.url}`);
      return null;
    }
  } else {
    logger.debug(`[ExtractCoverImage] Load image from cache at : ${filePath}`);
  }
  return filePath;
};

module.exports = {
  fetchOnline,
  extractDomain,
  extractTitle,
  extractFavicon,
  extractDescription,
  resolveUrl,
  extractCoverImageForBookmark,
};
