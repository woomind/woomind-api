module.exports = {
  // Database configurations.
  database: {
    url: 'WOOMIND_API_DB_URL',
    port: 'WOOMIND_API_DB_PORT',
    name: 'WOOMIND_API_DB_NAME',
    user: 'WOOMIND_API_DB_USER',
    password: 'WOOMIND_API_DB_PASSWORD',
  },

  // http configurations.
  http: {
    enable: 'WOOMIND_API_ENABLE_HTTP',
    port: 'WOOMIND_API_HTTP_PORT',
  },

  // https configurations.
  https: {
    enable: 'WOOMIND_API_ENABLE_HTTPS',
    port: 'WOOMIND_API_HTTPS_PORT',
    privateKey: 'WOOMIND_API_HTTPS_PRIVATE_KEY_PATH',
    certificate: 'WOOMIND_API_HTTPS_CERTIFICATE_PATH',
  },

  // Logs.
  logs: {
    path: 'WOOMIND_API_LOG_PATH',
    level: 'WOOMIND_API_LOG_LEVEL',
  },

  // Resources.
  resources: {
    path: 'WOOMIND_API_RESOURCES_PATH',
  },

  // Security configurations.
  security: {
    salt: 'WOOMIND_SALT',
    password_entropy: 'WOOMIND_PASSWORD_ENTROPY',
    access_token_lifetime: 'WOOMIND_ACCESS_TOKEN_LIFETIME',
    access_token_cleanup: 'WOOMIND_ACCESS_TOKEN_CLEANUP',
  },
};
