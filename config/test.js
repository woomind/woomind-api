module.exports = {
  // Database configurations.
  database: {
    url: 'localhost',
    port: 27017,
    name: 'woomind-tests',
  },

  // http configurations.
  http: {
    enable: false,
    port: 3000,
  },
  // https configurations.
  https: {
    enable: true,
    port: 3443,
    privateKey: './certfiles/private.key',
    certificate: './certfiles/certificate.cert',
  },

  // Logs.
  logs: {
    path: './logs',
    level: 'none',
  },

  // Security configurations.
  security: {
    salt: 'dummy_salt', // Replace by your own, randomly generated salt.
    password_entropy: 45,
    access_token_lifetime: 60 * 60 * 1000 * 24, // 24 hour expressed in ms
    access_token_cleanup: 60 * 60 * 1000 * 2, // 2 hour expressed in ms
  },
};
