# Welcome to the WooMind API!

## Prerequisites

- [Node.js](https://nodejs.org)
- [MongoDB](https://www.mongodb.com)

For help on installing nodeJS, see [Installation steps for Linux](https://github.com/nodejs/help/wiki/Installation).

For convenience, make sure to also copy `node`, `npm` and `npx` in the `$PATH` under `/usr/local/bin`.

## Recommended development tools for contributors

- [MongoDB Compass](https://www.mongodb.com/products/compass)
- [Nodemon](https://github.com/remy/nodemon)
- [Git flow](https://danielkummer.github.io/git-flow-cheatsheet)

## Get the project

### Clone the repository

```
$ git clone https://gitlab.com/woomind/woomind-api.git
```

### Checkout the `develop` branch

```
$ git checkout develop
```

### Build the project locally

```
$ npm install
```

## Run the project locally

### Prepare your install

#### Complete your configs

Copy and rename `development.example` file to `development.json` or `production.json` depending on your current
environment.  
Edit the newly created file to match your installation. In particular, you might have different database configurations.

NOTE: mongoDB installs WITHOUT authentication by default. To avoid using it, you should remove the two following lines
in the `database` section:

```
"user": "admin",
"password": "admin"
```

Please, mind, the JSON structure and remove the `,` on previous line if needed.

#### Edit your hosts file

You might want to bind a friendly URL to the project. To do so, update your `hosts` file to add the following line:

```
127.0.0.1 api.woomind.local
```

### Start the server

```
$ node start
```

NOTES: if you need help creating data, you can use `node seed` to seed some data in your mongoDB database.

### Query endpoints

We recommend using [Postman](https://www.getpostman.com/) to query endpoints.

```
GET http://api.woomind.local:3000/api/bookmarks
```

### Run the tests

Unsure if everything all right ? Run the tests coverage on the project using:

```
$ npm test
```

### Use HTTPS

You can use OpenSSL to self sign your application using the following command:

```
openssl req -x509 -newkey rsa:2048 -keyout certfiles/key.pem -out certfiles/cert.pem -days 365
```

Change your environment.js file to force all routes to https via:

```
https: {
    force: true,
}
```
