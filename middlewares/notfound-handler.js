/**
 * Error handler middleware.
 * Logs errors occurring during a request process.
 */
const router = require('express-promise-router')();
const logger = require('../services/logger');

// Handle errors during process.
router.all('*', async (req, res) => {
  logger.debug('Route not found');
  res.status(404).send('Route not found');
});

module.exports = router;
