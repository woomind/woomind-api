/**
 * Error handler middleware.
 * Logs errors occurring during a request process.
 */

const logger = require('../services/logger');
const { InternalServerError } = require('../utils/errors');

// Handle errors during process.
module.exports = (err, req, res, next) => {
  let error = err;

  // Change to custom error format type.
  if (error.status === undefined) {
    error = new InternalServerError(error);
  }

  logger.error(`[${error.type}] ${error.status}: ${error.message}, ${error.description}`);
  if (error.status === 500) logger.error(error.stack);

  res.status(error.status).send({
    error,
  });

  return next();
};
