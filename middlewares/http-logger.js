/**
 * Defines a middleware to log all incoming requests at different levels.
 */

const logger = require('../services/logger');

/**
 * Determine a log level depending on given HTTP status.
 *
 * @param statusCode (integer)
 * @return string
 */
const getLogLevelPerStatusCode = (statusCode) => {
  if (statusCode >= 500) {
    return 'error';
  }
  if (statusCode >= 400) {
    return 'warn';
  }
  return 'info';
};

/**
 * Applies to all routes: log incoming requests.
 */
module.exports = (req, res, next) => {
  // Log incoming request as info: GET /url
  logger.info(`${req.method} ${req.originalUrl} (${req.protocol})`);
  // If silly enabled: log all headers.
  if (logger.logLevel === 'silly') {
    Object.keys(req.headers).forEach((key) => {
      const header = key + ' '.repeat(Math.max(0, 10 - key.length));
      logger.silly(`\tHEADER ${header}\t=>\t${req.headers[key]}`);
    });
  }
  logger.silly(`\tBODY ${JSON.stringify(req.body)}`);

  // Successful pipeline (regardless of its response)
  res.on('finish', () => {
    const logLevel = getLogLevelPerStatusCode(res.statusCode);
    logger.log(
      logLevel,
      `End of ${req.method} ${req.originalUrl} : ${res.statusCode} ${res.statusMessage}; ${
        res.get('Content-Length') || 0
      }b sent`
    );
  });

  next();
};
