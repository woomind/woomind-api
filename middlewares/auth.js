const config = require('config');
const passport = require('passport');
// const FacebookTokenStrategy = require('passport-facebook-token');
const { BasicStrategy } = require('passport-http');
const { Strategy: ClientPasswordStrategy } = require('passport-oauth2-client-password');
const { Strategy: BearerStrategy } = require('passport-http-bearer');
const { secureCors } = require('../utils/security');
const Client = require('../models/client');
const User = require('../models/user');
const AccessToken = require('../models/access_token');
const logger = require('../services/logger');

// HELPERS
function verifyScopes(grantedScopes, requestedScopes) {
  // eslint-disable-next-line no-restricted-syntax
  for (const scope of requestedScopes) {
    // Note: we have defined our scope as a '$key:$value' pair.
    // The $value can be either 'read' or 'all' with the latest superseeds the first.
    // The $key can exist in singular or plural : plural meaning 'not just mine' so superseeds singular.
    //
    // For instance :
    //
    // - 'user:read' means : 'can access my own profile'.
    // - 'user:all' means : 'can access / update / delete my own profile'.
    // - 'users:all' means : 'can see / update / delete any user'.

    const [key, value] = scope.split(':');

    // If the value is 'all', we can accept only accept '$key:all' and '$keys:all'.
    if (value === 'all') {
      if (grantedScopes.indexOf(`${key}:all`) < 0 && grantedScopes.indexOf(`${key}s:all`) < 0) {
        logger.debug(
          `[Scope checks] User is not authorized for the following scope: ${key}:${value}`
        );
        return false;
      }
    }
    // If the value is 'read', we can accept accept '$key:read', '$key:all' and '$keys:all'.
    else if (
      grantedScopes.indexOf(`${key}:read`) < 0 &&
      grantedScopes.indexOf(`${key}:all`) < 0 &&
      grantedScopes.indexOf(`${key}s:all`) < 0
    ) {
      logger.debug(
        `[OAuth2 Password Grant] User is not authorized for the following scope: ${key}:${value}.`
      );
      return false;
    }
  }
  return true;
}

/**
 * BasicStrategy & ClientPasswordStrategy
 *
 * These strategies are used to authenticate registered OAuth clients.  They are
 * employed to protect the `token` endpoint, which consumers use to obtain
 * access tokens.  The OAuth 2.0 specification suggests that clients use the
 * HTTP Basic scheme to authenticate.
 *
 * Note : Use of the client password strategy allows clients to send the same credentials in the request body
 * (as opposed to the `Authorization` header).  While this approach is not recommended by the specification,
 * in practice it is quite common, so I decided to had it still for third-party future consumers.
 */
passport.use(
  new BasicStrategy((clientId, clientSecret, done) => {
    logger.debug(
      '[Passport BasicStrategy] Validate client from clientId/clientSecret retrieved in Basic header.'
    );
    logger.silly(`[Passport BasicStrategy] clientId=${clientId} clientSecret=${clientSecret}`);
    Client.findOne({ clientId }, (err, client) => {
      if (err) {
        logger.debug('[Passport BasicStrategy] Client.findOne query did not performed.');
        return done(err);
      }
      if (!client) {
        logger.debug('[Passport BasicStrategy] No such client found.');
        return done(null, false);
      }
      if (client.clientSecret !== clientSecret) {
        logger.debug('[Passport BasicStrategy] Client found but secret missmatch');
        return done(null, false);
      }

      logger.debug('[Passport BasicStrategy] Client is now authorized.');
      return done(null, client);
    });
  })
);
passport.use(
  new ClientPasswordStrategy((clientId, clientSecret, done) => {
    logger.debug(
      '[Passport ClientPasswordStrategy] Validate client from clientId/clientSecret retrieved in Basic header.'
    );
    logger.silly(
      `[Passport ClientPasswordStrategy] clientId=${clientId} clientSecret=${clientSecret}`
    );
    Client.findOne({ clientId }, (err, client) => {
      if (err) {
        logger.debug('[Passport ClientPasswordStrategy] Client.findOne query did not performed.');
        return done(err);
      }
      if (!client) {
        logger.debug('[Passport ClientPasswordStrategy] No such client found.');
        return done(null, false);
      }
      if (client.clientSecret !== clientSecret) {
        logger.debug('[Passport ClientPasswordStrategy] Client found but secret missmatch');
        return done(null, false);
      }

      logger.debug('[Passport ClientPasswordStrategy] Client is now authorized.');
      return done(null, client);
    });
  })
);

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate a registered user. To do so, it validates the AccessToken presented via the
 * Bearer Authorize header.
 *
 * Note : At this step, we don't validate the AccessToken scopes regarding the operation requested. This will be the
 * purpose of a later middleware.
 */
passport.use(
  new BearerStrategy((token, done) => {
    logger.debug('[Passport BearerStrategy] Validate client/user from posted Bearer AccessToken.');

    // eslint-disable-next-line consistent-return
    AccessToken.findOne({ token }, (err, accessToken) => {
      if (err) {
        logger.debug('[Passport BearerStrategy] AccessToken.findOne query did not performed.');
        return done(err);
      }
      if (!accessToken) {
        logger.debug('[Passport BearerStrategy] No such accessToken found.');
        return done(null, false);
      }

      // Validate expirationDate.
      if (accessToken.expirationDate.getTime() < new Date().getTime()) {
        logger.debug('[Passport BearerStrategy] AccessToken is expired.');
        return done(null, false);
      }

      logger.debug('[Passport BearerStrategy] Access found : getting user from it.');

      // eslint-disable-next-line consistent-return
      User.findById(accessToken.userId, (errUser, user) => {
        if (errUser) {
          logger.debug('[Passport BearerStrategy] User.findById query did not performed.');
          return done(err);
        }
        if (!user) {
          logger.debug('[Passport BearerStrategy] No user found associated with this accessToken.');
          return done(null, false);
        }

        Client.findOne({ clientId: accessToken.clientId }, (errClient, client) => {
          if (errClient) {
            logger.debug('[Passport BearerStrategy] Client.findById query did not performed.');
            return done(err);
          }
          if (!client) {
            logger.debug(
              '[Passport BearerStrategy] No client found associated with this accessToken.'
            );
            return done(null, false);
          }

          // @todo evaluate this strategy (took inspiration on Facebook Login API)
          logger.debug('[Passport BearerStrategy] concurrently renew AccessToken.');
          // eslint-disable-next-line no-param-reassign
          accessToken.expirationDate = new Date(
            new Date().getTime() + config.get('security.access_token_lifetime')
          );
          accessToken.save();

          logger.debug('[Passport BearerStrategy] User and Client is now authorized.');
          return done(null, user.toObject({ virtuals: true }), {
            client,
            token: accessToken,
          });
        });
      });
    });
  })
);

exports.verifyScopes = verifyScopes;
exports.isClientAuthentified = [
  passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
  secureCors,
];
exports.isUserAuthentified = [passport.authenticate('bearer', { session: false }), secureCors];
exports.isUserAllowed = (scopes) => {
  return (req, res, next) => {
    logger.logComplexData(
      scopes,
      '[Auth isUserAllowed] Check if users can perform following operation.',
      'debug'
    );
    if (!verifyScopes(req.authInfo.token.scope, scopes)) {
      res.status(401);
      return res.send('Unauthorized');
    }
    return next();
  };
};
