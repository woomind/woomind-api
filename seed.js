const Client = require('./models/client');
const User = require('./models/user');
const AccessToken = require('./models/access_token');
const Bookmark = require('./models/bookmark');
const Resource = require('./models/resource');
const router = require('./routes/v2/bookmarks');
require('./startup/database');

const NUMBER_BOOKMARKS = 0;
const users = [
  {
    email: 'admin@test.com',
    password: 'Pa$$w0rd!',
    isAdmin: true,
    scope: ['users:all', 'bookmarks:all'],
  },
  {
    email: 'test@test.com',
    password: 'Pa$$w0rd!',
    scope: ['user:all', 'bookmark:all'],
  },
];

const clients = [
  {
    name: 'dummy_client',
    description: 'Just a dummy client.',
    clientId: 'client_trusted',
    clientSecret: 'secret_client_trusted',
    domain: 'https://woomind.local:9000',
    platform: 'web',
    grants: ['password'],
  },
];

// ----------
// HELPERS: create entities.

async function createClients() {
  return clients.map(async (client) => {
    // eslint-disable-next-line no-return-await
    return await new Client(client).save();
  });
}

async function createUsers() {
  const createdUsers = [];
  for (let i = 0; i < users.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const newUser = await new User(users[i]).save();
    createdUsers.push({
      uid: newUser.uid,
      ...newUser.toObject(),
    });
  }
  return createdUsers;
}

async function createBookmarks(uid) {
  for (let i = 0; i < NUMBER_BOOKMARKS; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    await router.createBookmark(uid, {
      uid,
      url: `https://www.google.com/search?q=${i}`,
      tags: ['dummy', 'foo', 'bar'],
      flags: {
        favorite: false,
        read_later: false,
        archive: false,
      },
    });
  }
}

// ---------
async function seed() {
  // Remove all.
  await Client.deleteMany({});
  await User.deleteMany({});
  await AccessToken.deleteMany({});
  await Bookmark.deleteMany({});
  await Resource.deleteMany({});

  // Create clients.
  const createdClients = await createClients();
  console.log('Created number of clients:', createdClients.length);

  // Create users.
  const createdUsers = await createUsers();
  console.log('Created number of users:', createdUsers.length);

  // Create bookmrks.
  await createdUsers.map(async (user) => {
    await createBookmarks(user.uid);
  });
  // await mongoose.disconnect();
}

// ---------
// Run seed.

seed().then(
  () => {
    console.log('Seed Done');
  },
  (err) => {
    console.log('Seed fail');
    console.log(err);
  }
);
