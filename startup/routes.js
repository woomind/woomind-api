/**
 * Configure routes.
 * For readability, routes about each entity are defined in a routes/...js file.
 */

// Imports.
const express = require('express');
const helmet = require('helmet');
const passport = require('passport');
const httpLogger = require('../middlewares/http-logger');
const error = require('../middlewares/error-handler');
const notFound = require('../middlewares/notfound-handler');
const { enableCors } = require('../utils/security');
// Import routes to API v1.
const general = require('../routes/general');
// Import routes to API v2.
const bookmarks = require('../routes/v2/bookmarks').router;
const fetchProxy = require('../routes/v2/fetchProxy');
const oauth = require('../routes/v2/oauth2');
const users = require('../routes/v2/users');

// Export routes and attach specific middleware.
module.exports = (app) => {
  // All routes middleware.
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(helmet());
  app.use(httpLogger);
  app.use(passport.initialize());

  // Allow CORS for all.
  // This will let all pre-route errors (404, 401, etc...) be accessible to client (since cors security will only run
  // after route safety checks [see auth.js]).
  // This will also allow OPTIONS requests: allowing prefetch for all clients.
  app.use('*', enableCors);

  // General routes.
  app.use('/', general);

  // Version 1 API : dropped support.

  // Version 2 API.
  app.use('/v2/oauth', oauth);
  app.use('/v2/users', users);
  app.use('/v2/bookmarks', bookmarks);
  app.use('/v2/fetch', fetchProxy);

  // Fallback to latest stable available API version (currently v2)
  app.use('/oauth', oauth);
  app.use('/users', users);
  app.use('/bookmarks', bookmarks);
  app.use('/fetch', fetchProxy);

  // Error handling.
  app.use(notFound);
  app.use(error);
};
