/**
 * Entry point for WooMind API application.
 */

// Imports
const express = require('express');

// Build app
const app = express();

// Registers middleware.
require('./config')();
const logger = require('../services/logger');
require('./routes')(app);
const db = require('./database');

logger.info(`== RUNNING IN ${process.env.NODE_ENV} MODE ==`);
module.exports = { app, db };
