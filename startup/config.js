/**
 * Startup for config module.
 */

const config = require('config');

module.exports = () => {
  if (
    config.get('https.enable') &&
    (!config.has('https.privateKey') ||
      !config.get('https.privateKey') ||
      !config.has('https.certificate') ||
      !config.get('https.certificate'))
  ) {
    throw new Error(
      'FATAL ERROR: certfiles are not defined. Please consult README within certfiles folder.'
    );
  }

  if (
    !config.has('database') ||
    !config.has('database.url') ||
    !config.get('database.url') ||
    !config.has('database.port') ||
    !config.get('database.port') ||
    !config.has('database.name') ||
    !config.get('database.name')
  ) {
    throw new Error('FATAL ERROR: database connection info are not defined');
  }
};
