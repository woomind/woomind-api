/**
 * Startup for mongoose module:
 * Initializes database connection.
 */

// Imports.
const Database = require('../services/database');

Database.start();

module.exports = Database;
