const fs = require('fs');
const path = require('path');
const config = require('config');
const logger = require('../services/logger');

/**
 * Get the resources folder directory.
 * @returns {*|string}
 */
const getResourcesFolder = () => {
  if (config.resources && config.resources.path) {
    return config.resources.path;
  }
  return `${path.dirname(require.main.filename)}/resources`;
};

/**
 * Creates a folder.
 *
 * @param dir
 *      The folder dir.
 */
const createFolder = (dir) => {
  if (!fs.existsSync(dir)) {
    logger.debug(`[createFolder] Create the folder at: ${dir}`);
    fs.mkdirSync(dir, { recursive: true });
  }
};

/**
 * Delete a folder and it's content.
 *
 * @param dir
 *      The folder dir.
 */
const deleteFolder = (dir) => {
  logger.debug(`[deleteFolder] Delete the folder at: ${dir}`);
  // If the dir exists, clean it.
  if (fs.existsSync(dir)) {
    fs.readdirSync(dir).forEach((file) => {
      const curPath = `${dir}/${file}`;
      if (fs.lstatSync(curPath).isDirectory()) {
        // Recursively delete sub-folders content.
        deleteFolder(curPath);
      } else {
        // Delete file.
        fs.unlinkSync(curPath);
      }
    });
    // Remove dir.
    fs.rmdirSync(dir);
  }
};

/**
 * Delete the given folder and all hierarchy recursively if empty.
 *
 * @param dir
 */
const cleanHierarchy = (dir) => {
  // If the parent dir is empty, clean it recursively until resources dir is reach.
  const parentDir = path.resolve(dir, '..');
  logger.debug(`[CleanHierarchy] Clean hierarchy folder at: ${dir}`);
  if (parentDir !== getResourcesFolder() && !fs.readdirSync(parentDir).length) {
    // Delete folder.
    fs.rmdirSync(parentDir);
    // Recursively delete parent-folder content.
    setTimeout(() => cleanHierarchy(parentDir), 50);
  }
};

/**
 * Read content of a temporary file.
 */
const readTmpFile = (filename) => {
  return fs.readFileSync(`${getResourcesFolder()}/tmp/${filename}`, 'utf-8');
};

module.exports = {
  deleteFolder,
  createFolder,
  cleanHierarchy,
  getResourcesFolder,
  readTmpFile,
};
