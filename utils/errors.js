class InternalServerError extends Error {
  constructor(errors, ...params) {
    super(...params);
    this.status = 500;
    this.type = 'INTERNAL_SERVER_ERROR';
    this.message = 'Internal Server Error';
  }
}

class FileParsingError extends Error {
  constructor(message) {
    super();
    this.status = 400;
    this.type = 'FILE_READ_ERROR';
    this.message = message || 'Invalid file.';
  }
}

class FileTypeError extends Error {
  constructor(message, ...params) {
    super(...params);
    this.status = 400;
    this.type = 'FILE_TYPE_ERROR';
    this.message = 'Invalid file type';
    this.description = message || 'This filetype is not authorized.';
  }
}

class InputValidationError extends Error {
  constructor(errors, ...params) {
    super(...params);
    this.status = 400;
    this.type = 'INPUT_VALIDATION_ERROR';
    this.message = 'One or more fields raised validation errors';
    this.fields = {};

    // Iterates on return JOI errors.
    for (let i = 0; i < errors.length; i += 1) {
      this.fields[errors[i].path[0]] = errors[i].message;
    }
  }
}

class EntityAlreadyExistsError extends Error {
  constructor(entityName, query, ...params) {
    super(...params);
    this.status = 400;
    this.type = `${entityName.trim().toUpperCase().replace(/ /g, '_')}_ALREADY_EXISTS`;
    this.message = `${entityName} already exists`;
    this.fields = {};

    // Iterates query fields.
    const keys = Object.keys(query);
    for (let i = 0; i < keys.length; i += 1) {
      this.fields[keys[i]] = `This ${keys[i]} is already used. Please try another.`;
    }
  }
}

class EntityNotFoundError extends Error {
  constructor(entityName, query = null, ...params) {
    super(...params);
    this.status = 404;
    this.message = `${entityName} not found`;
    this.type = `${entityName.trim().toUpperCase().replace(/ /g, '_')}_NOT_FOUND`;
    this.fields = {};

    // Iterates query fields.
    if (query && query.fields) {
      const keys = Object.keys(query.fields);
      for (let i = 0; i < keys.length; i += 1) {
        this.fields[keys[i]] = `${entityName} not found: please try another ${keys[i]} value.`;
      }
    }
  }
}

class AuthenticationFailedError extends Error {
  constructor(errors, ...params) {
    super(...params);
    this.status = 403;
    this.type = 'AUTHENTICATION_ERROR';
    this.message = 'Your email or password is incorrect.';
  }
}

class AccessDeniedError extends Error {
  constructor(errors, ...params) {
    super(...params);
    this.status = 401;
    this.type = 'ACCESS_DENIED';
    this.message = "You don't have permissions to perform this operation.";
  }
}

module.exports = {
  InputValidationError,
  EntityAlreadyExistsError,
  EntityNotFoundError,
  AuthenticationFailedError,
  AccessDeniedError,
  InternalServerError,
  FileTypeError,
  FileParsingError
};
