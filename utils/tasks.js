const tasks = {};

/**
 * Remove a given task from task list.
 * @param taskName
 * @param defer
 * @returns {boolean}
 */
function removeTask(taskName, defer = 0) {
  if (!(taskName in tasks)) {
    return false;
  }

  const deletedTask = tasks[taskName];

  if (defer) {
    setTimeout(() => {
      delete tasks[taskName];
    }, defer);
  } else {
    delete tasks[taskName];
  }

  return deletedTask;
}

/**
 * Add a tasks to the task list.
 * @param taskName
 * @param data
 * @param lifetime
 */
function addTask(taskName, data, lifetime = 0) {
  if (taskName in tasks) {
    return false;
  }
  tasks[taskName] = {
    createdAt: Date.now(),
    ...data,
  };

  if (lifetime) {
    setTimeout(() => {
      removeTask(taskName);
    }, lifetime);
  }

  return tasks[taskName];
}

/**
 * Retrieve a task from tasklist.
 */
function getTask(taskName) {
  if (!(taskName in tasks)) {
    return false;
  }
  return tasks[taskName];
}

module.exports = {
  addTask,
  getTask,
  removeTask,
};
