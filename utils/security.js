const cors = require('cors');
const config = require('config');
const crypto = require('crypto');
const fs = require('fs');

const algorithm = 'aes-256-cbc';
const salt = config.get('security.salt');

const privateKey = fs.readFileSync(config.get('https.privateKey'), 'utf8');
const certificate = fs.readFileSync(config.get('https.certificate'), 'utf8');

function encrypt(data) {
  const cryptoKey = crypto.scryptSync(privateKey, salt, 24);
  const cipher = crypto.createCipheriv(algorithm, cryptoKey);
  let encrypted = cipher.update(data, 'utf8', 'hex');
  encrypted += cipher.final();
  return encrypted.toString('hex');
}

function decrypt(data) {
  const cryptoKey = crypto.scryptSync(privateKey, salt, 24);
  const decipher = crypto.createDecipheriv(algorithm, cryptoKey);
  let result = decipher.update(data, 'hex', 'utf8');
  result += decipher.final('utf8');
  return result;
}

const enableCors = cors({ origin: true });

const secureCors = cors((req, callback) => {
  // The verified domain can be in the authInfo (case of Bearer authentication).
  let verifiedDomain = req.authInfo.client ? req.authInfo.client.domain : false;
  // But surprisingly, req.user can actually be a client (case of routes called by unknown user, for instance /login).
  verifiedDomain = verifiedDomain || (req.user ? req.user.domain : false);
  const corsOptions = { origin: verifiedDomain };
  callback(null, corsOptions);
});

module.exports = {
  privateKey,
  certificate,
  encrypt,
  decrypt,
  enableCors,
  secureCors,
};
