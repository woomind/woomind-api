const JobQueue = require('node-queue-runner');

/**
 * A class to queue import jobs.
 */
class ImportQueue extends JobQueue {
  constructor(userId) {
    // @todo evaluate the current strategy : run jobs at 2/sec bauderate.
    super(0, 500, true);
    this.userId = userId;
  }

  // eslint-disable-next-line class-methods-use-this
  async runJob(job) {
    // eslint-disable-next-line global-require
    const { createBookmark } = require('../routes/v2/bookmarks');
    return createBookmark(this.userId, job, { import: true });
  }
}

module.exports = ImportQueue;
