// Imports.
const Joi = require('joi');
const logger = require('../services/logger');
const {
  InputValidationError,
  EntityAlreadyExistsError,
  EntityNotFoundError,
} = require('../utils/errors');

module.exports.options = (id, validateBeforeSave = true) => {
  return {
    toObject: {
      transform: (doc, ret) => {
        // Remove the _id of every document before returning the result
        ret[id] = ret._id.toString();
        // Remove all virtual keys.
        Object.keys(ret).forEach((key) => {
          if (key.startsWith('_')) delete ret[key];
        });
      },
    },
    toJSON: {
      transform: (doc, ret) => {
        // Remove the _id of every document before returning the result
        ret[id] = doc._id.toString();
        // Remove all virtual keys.
        Object.keys(ret).forEach((key) => {
          if (key.startsWith('_')) delete ret[key];
        });
        return ret;
      },
    },
    validateBeforeSave,
  };
};

module.exports.statics = {
  /**
   * Find an Entity from query
   *
   * @param query (string|object) An entity ID or a query object.
   */
  async findBy(query) {
    // Transform query.
    const request = typeof query === 'string' || query instanceof String ? { _id: query } : query;
    return this.findOne(request);
  },

  /**
   * Updates a entity by ID or query.
   * @param query (string|object) The query, or the ID.
   * @param data (object) the data to update entity with.
   * @returns {Promise<*>}
   */
  async update(query, data) {
    const cond =
      Object.prototype.toString.call(query) === '[object String]' ? { _id: query } : query;

    logger.debug(
      `[Update${this.modelName}] Update entity by condition ${JSON.stringify(cond)} in storage.`
    );

    const updatedEntity = await this.findOneAndUpdate(cond, data, {
      new: true,
      runValidators: true,
    });

    if (updatedEntity) {
      logger.debug(`[Update${this.modelName}] Entity has been updated`);
      logger.logComplexData(
        updatedEntity.toJSON(),
        `[Update${this.modelName}] Updated entity data:`,
        'silly'
      );
    } else {
      logger.debug(`[Update${this.modelName}] Entity not found`);
    }
    return updatedEntity;
  },

  /**
   * Delete a entity.
   * @param query (string|object) The query, or the ID.
   * @returns {Promise<*>}
   */
  async delete(query) {
    const cond =
      Object.prototype.toString.call(query) === '[object String]' ? { _id: query } : query;

    logger.debug(
      `[Delete${this.modelName}] Delete entity by condition ${JSON.stringify(cond)} in storage.`
    );

    // NOTE: Using pretty weird syntax here rather than findOneAndRemove() because we need to make
    // sure post hooks are run and remove() fires it.
    // This is necessary in post remove scenarios, for instance to delete Bookmarks of a user when
    // a User is deleted.
    const deleteEntity = await this.findOne(cond, (err, entity) => {
      if (!err && entity) {
        entity.remove();
      }
    });

    if (deleteEntity) {
      logger.debug(`[Delete${this.modelName}] Entity has been deleted`);
      logger.logComplexData(
        deleteEntity.toJSON(),
        `[Delete ${this.modelName} ] Deleted entity data:`,
        'silly'
      );
    } else {
      logger.debug(`[Delete${this.modelName}] Entity not found`);
      throw new EntityNotFoundError('Bookmark');
    }

    return deleteEntity;
  },

  /**
   * Delete a entity.
   * @param query (string|object) The query, or the ID.
   * @returns {Promise<*>}
   */
  async deleteMultiple(query) {
    const cond =
      Object.prototype.toString.call(query) === '[object String]' ? { _id: query } : query;

    logger.debug(
      `[DeleteMultiple${this.modelName}] Delete entities by condition ${JSON.stringify(
        cond
      )} in storage.`
    );

    // NOTE: Using pretty weird syntax here rather than findOneAndRemove() because we need to make
    // sure post hooks are run and remove() fires it.
    // This is necessary in post remove scenarios, for instance to delete Bookmarks of a user when
    // a User is deleted.
    const deleteEntities = await this.find(cond, (err, docs) => {
      if (!err && docs) {
        docs.forEach((doc) => doc.remove());
      }
    });

    if (deleteEntities) {
      logger.debug(
        `[DeleteMultiple${this.modelName}] ${deleteEntities.length} entities has been deleted`
      );
    } else {
      logger.debug(`[DeleteMultiple${this.modelName}] Entities not found`);
      throw new EntityNotFoundError('Bookmark');
    }

    return deleteEntities;
  },

  //--------------------------------------------------------------------------------
  // CHECKS
  //
  // Checks are used in routes and returns either an expected object or throws an
  // error with a message and an HTTP corresponding error.
  //--------------------------------------------------------------------------------

  /**
   * Checks the input data validity against entity schema.
   * @param input
   * @param context an context object.
   * @returns boolean: Joi validation.
   */
  async checkInputValidity(input, context = {}) {
    const { error } = Joi.validate(input, this.validationSchema, {
      abortEarly: false,
      // stripUnknown: true,
      context,
    });

    logger.debug(
      `[CheckInputValidity] ${this.modelName} input data validation [with context: ${JSON.stringify(
        context
      )}.`
    );

    if (error) {
      logger.logComplexData(error, '[CheckInputValidity] Errors', 'debug');
      throw new InputValidationError(error.details);
    }
    return true;
  },

  /**
   * Check for entity presence in database.
   *
   * @param query (string|object)
   *   The Id or an array value to query the entity upon.
   * @param desiredPresence (boolean)
   * @param exception The _id of a entity to exclude from possible existing ones. Useful to check
   * that no entity exists yet, 'but me'.
   *
   * @returns {Promise<*>}: Entity if existing, FALSE otherwise.
   */
  async checkExists(query, desiredPresence = true, exception = undefined) {
    logger.debug(
      `[CheckExisting${this.modelName}] Check if entity exists: required ${desiredPresence}`
    );

    // Find entity.
    const entity = await this.findBy(query);

    if (desiredPresence && !entity) {
      throw new EntityNotFoundError(this.modelName, query);
    }
    if (!desiredPresence && entity && exception !== entity._id) {
      throw new EntityAlreadyExistsError(this.modelName, query);
    }

    if (entity) {
      logger.logComplexData(
        entity.toJSON(),
        `[CheckExisting${this.modelName}] ${this.modelName} found:`
      );
    }
    return entity;
  },
};
