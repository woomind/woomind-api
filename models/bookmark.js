/**
 * Model for Bookmark entity.
 */

// Imports.
const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const Joi = require('joi');
const urlChecker = require('regex-weburl');
const baseModel = require('./base');
const logger = require('../services/logger');

const MAX_TAGS_NUMBER = 20;
const MAX_TAG_SIZE = 25;
const FLAG_CONSTANTS = {
  read: 0b1, // 1
  favorite: 0b10, // 2
  archive: 0b100, // 4
  read_later: 0b1000, // 8
};

const bookmarkSchema = new mongoose.Schema(
  {
    uid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    title: {
      type: String,
      trim: true,
      required: true,
    },
    url: {
      type: String,
      trim: true,
      required: true,
    },
    description: {
      type: String,
      trim: true,
      required: false,
    },
    rid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Resource',
      required: true,
    },
    content: {
      type: String,
      trim: true,
    },
    favicon: {
      type: String,
      trim: true,
    },
    domain: {
      type: String,
      trim: true,
    },
    flags: {
      favorite: {
        type: Boolean,
        default: false,
      },
      read_later: {
        type: Boolean,
        default: false,
      },
      archive: {
        type: Boolean,
        default: false,
      },
    },
    tags: {
      type: [
        {
          type: String,
          trim: true,
          max: MAX_TAG_SIZE,
        },
      ],
      required: true,
      max: MAX_TAGS_NUMBER,
    },
    hash: {
      type: String,
      required: true,
    },
  },
  baseModel.options('bid', false)
);

// Add index strings.
bookmarkSchema.index({
  title: 'text',
  description: 'text',
  url: 'text',
  content: 'text',
  domain: 'text',
  tags: 'text',
});

// Add validation schema.
bookmarkSchema.statics.validationSchema = {
  title: Joi.string().trim().max(255).allow('').optional(),
  url: Joi.string()
    .regex(urlChecker, 'valid')
    .trim()
    .when('$patch', {
      is: Joi.boolean().valid(true).required(),
      then: Joi.optional(),
      otherwise: Joi.required(),
    }),
  description: Joi.string().trim().max(300).allow('').optional(),
  content: Joi.string().trim().optional(),
  favicon: Joi.string().uri().trim().optional(),
  illustration: Joi.object({
    src: Joi.string().uri().trim().required(),
    alt: Joi.string().optional(),
  }).optional(),
  flags: Joi.object()
    .pattern(
      Joi.string().required().valid(['read', 'archive', 'favorite', 'read_later']),
      Joi.boolean().required()
    )
    .when('$put', {
      is: Joi.boolean().valid(true).required(),
      then: Joi.required(),
      otherwise: Joi.optional(),
    }),
  tags: Joi.array()
    // .items(Joi.string().trim().regex(/^\S+$/, 'no whitespace policy').max(MAX_TAG_SIZE))
    .items(Joi.string().lowercase().trim().max(MAX_TAG_SIZE))
    .max(MAX_TAGS_NUMBER)
    .when('$put', {
      is: Joi.boolean().valid(true).required(),
      then: Joi.required(),
      otherwise: Joi.optional(),
    }),
  createdAt: Joi.when('$import', {
    is: Joi.boolean().valid(true).required(),
    then: Joi.optional(),
    otherwise: Joi.forbidden(),
  }),
};

// Add common model methods.
bookmarkSchema.static(baseModel.statics);

// Add virtual methods.
bookmarkSchema.virtual('bid').get(() => {
  return this._id;
});

/**
 * Delete a user should delete associated models and data.
 */
bookmarkSchema.post('remove', { query: true, document: true }, async (doc, next) => {
  logger.debug('[BookmarkModel] Fire post remove hook: remove associated ressources.');
  // eslint-disable-next-line no-shadow,global-require
  const { createOrUpdateResource } = require('../routes/v2/bookmarks');
  createOrUpdateResource(doc.toJSON(), -1);
  next();
});

bookmarkSchema.plugin(timestamps);
const Bookmark = mongoose.model('Bookmark', bookmarkSchema);
Bookmark.MAX_TAGS_NUMBER = MAX_TAGS_NUMBER;

module.exports = Bookmark;
