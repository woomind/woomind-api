/**
 * Model for Client entity.
 *
 * Client should be understood in scope of OAuth2 :
 * A third-party application requesting access to our resources in the name of a user.
 */

const mongoose = require('mongoose');
const baseModel = require('./base');

const ClientSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  clientId: {
    type: String,
    unique: true,
    required: true,
  },
  clientSecret: {
    type: String,
    required: true,
  },
  domain: {
    type: String,
    trim: true,
    required: true,
  },
  scope: {
    type: [
      {
        type: String,
        trim: true,
      },
    ],
    required: true,
    default: ['user:all', 'bookmark:all'],
  },
  grants: {
    type: [
      {
        type: String,
        trim: true,
      },
    ],
    required: true,
  },
});

// Add common model methods.
ClientSchema.static(baseModel.statics);

const Client = mongoose.model('Client', ClientSchema);
module.exports = Client;
