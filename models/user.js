/**
 * Model for User entity.
 */

// Imports.
const mongoose = require('mongoose');
const Joi = require('joi');
const bcrypt = require('bcryptjs');
const config = require('config');
const stringEntropy = require('fast-password-entropy');
const isPasswordBlacklisted = require('password-blacklist');
const timestamps = require('mongoose-unix-timestamp-plugin');
const baseModel = require('./base');
const logger = require('../services/logger');
const Bookmark = require('./bookmark');
const AccessToken = require('./access_token');
const { InputValidationError, AuthenticationFailedError } = require('../utils/errors');

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 255,
      trim: true,
      unique: true,
    },
    hashedPassword: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 1024,
      trim: true,
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
    scope: {
      type: [
        {
          type: String,
          trim: true,
        },
      ],
      required: true,
      default: ['user:all', 'bookmark:all'],
    },
  },
    baseModel.options('uid')
);

// Add validation schema.
userSchema.statics.validationSchema = {
  email: Joi.string()
    .min(5)
    .max(255)
    .regex(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/, 'email format')
    .trim()
    .lowercase()
    .when('$patch', {
      is: Joi.boolean().valid(true).required(),
      then: Joi.optional(),
      otherwise: Joi.required(),
    }),
  password: Joi.string()
    .trim()
    .max(1024)
    .when('$patch', {
      is: Joi.boolean().valid(true).required(),
      then: Joi.string().optional(),
      otherwise: Joi.string().required(),
    })
    .when('$login', {
      is: Joi.boolean().valid(true).required(),
      then: Joi.string().required(),
      otherwise: Joi.string().min(8),
    }),
};

/**
 * Delete a user should delete associated models and data.
 */
userSchema.post('remove', { query: true, document: true }, async (doc, next) => {
  logger.debug('[UserModel] Fire post remove hook: remove associated bookmarks.');
  await Bookmark.deleteMultiple({ uid: doc._id });
  logger.debug('[UserModel] Fire post remove hook: remove associated tokens.');
  await AccessToken.deleteMultiple({ userId: doc._id });
  next();
});

// Add common model methods.
userSchema.static(baseModel.statics);

// Add custom statics methods or overrides.
userSchema.static({
  /**
   * Check password against strength policies.
   * @param password
   * @returns {Promise<int>}: password strength
   */
  async checkPasswordStrength(password) {
    logger.debug('[Check] Validates password against blacklist.');
    logger.debug('[Check] Validates password entropy.');

    // Concurrent validation for performance.
    if (password) {
      const [blacklisted, entropy] = await Promise.all([
        // Validate password against blacklist.
        isPasswordBlacklisted(password),
        // Validate password entropy.
        stringEntropy(password),
      ]);

      // Validate password blacklisting.
      if (blacklisted) {
        throw new InputValidationError([
          {
            message: `Password is blacklisted.`,
            path: ['password'],
            type: 'password.blacklisted',
            context: {
              value: password,
              key: 'password',
              label: 'password',
            },
          },
        ]);
      }

      // Validate password entropy.
      const requiredEntropy = config.get('security.password_entropy') || 45;
      logger.silly(`[Check] Given password entropy is: ${entropy}.`);
      if (entropy < requiredEntropy) {
        throw new InputValidationError([
          {
            message: `Password is too weak. Current entropy is ${entropy}, requires above ${requiredEntropy}`,
            path: ['password'],
            type: 'password.entropy',
            context: {
              limit: requiredEntropy,
              value: entropy,
              key: 'password',
              label: 'password',
            },
          },
        ]);
      }
      return entropy;
    }

    throw new InputValidationError([
      {
        message: '"Password" is required.',
        path: ['password'],
        type: 'any.required',
        context: {
          key: 'password',
          label: 'password',
        },
      },
    ]);
  },

  /**
   * Check user password matches given password.
   * @param user The user to check password of.
   * @param password The password to check against.
   * @returns {Promise<*>} bcrypt matching promise.
   *
   * @deprecated
   */
  async checkPasswordMatch(user, password) {
    logger.debug(`[Check] Check password match.`);

    let match = false;
    if (password) {
      try {
        match = await bcrypt.compare(password, user.password);
      } catch (ex) {
        // Do nothing
      }
    }
    if (!password || !match) {
      throw new AuthenticationFailedError();
    }
    return match;
  },
});

// Add custom methods or overrides.
userSchema.method({
  /**
   * Compare the user password with the given password.
   */
  passwordMatch(password) {
    // eslint-disable-next-line no-return-await
    return bcrypt.compareSync(password, this.password);
  },
});

// Add virtual methods.
userSchema.virtual('uid').get(() => {
  return this._id;
});

userSchema
  .virtual('password')
  // eslint-disable-next-line func-names
  .set(function (password) {
    this.hashedPassword = bcrypt.hashSync(password, 10);
  })
  // eslint-disable-next-line func-names
  .get(function () {
    return this.hashedPassword;
  });

userSchema.plugin(timestamps);
const User = mongoose.model('User', userSchema);
module.exports = User;
