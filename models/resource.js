/**
 * Model for Resource entity.
 */

// Imports.
const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const baseModel = require('./base');

const resourceSchema = new mongoose.Schema(
  {
    url: {
      type: String,
      trim: true,
      unique: true,
      required: true,
    },
    cover: {
      type: String,
      trim: true,
    },
    cacheDir: {
      type: String,
      trim: true,
    },
    title: {
      type: String,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    content: {
      type: String,
      trim: true,
    },
    favicon: {
      type: String,
      trim: true,
    },
    usage: {
      type: Number,
    },
  },
  baseModel.options('rid')
);

// Add common model methods.
resourceSchema.static(baseModel.statics);
resourceSchema.plugin(timestamps);
const Resource = mongoose.model('Resource', resourceSchema);
module.exports = Resource;
