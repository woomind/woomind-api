/**
 * Model for RefreshToken entity.
 *
 * RefreshToken should be understood in scope of OAuth2 :
 * A token that allows a client to request a new AccessToken in the name of the user.
 *
 * @note I currently decided not to implement the RefreshToken part of OAuth specifications.
 *        The main reasons over this choice is :
 *        - some OAuth client seems to not support that part.
 *        - Facebook Login API seems to not support it but rather auto-reschedule
 *          the expiration as long as the access_token is used.
 * @todo Delete this once we are sure we don't want RefreshToken anymore.
 */

const mongoose = require('mongoose');

const RefreshTokenSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  clientId: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    unique: true,
    required: true,
  },
  expirationDate: {
    type: Date,
    default: Date.now,
  },
  scope: {
    type: [
      {
        type: String,
        trim: true,
      },
    ],
    required: true,
  },
});

const RefreshToken = mongoose.model('RefreshToken', RefreshTokenSchema);
module.exports = RefreshToken;
