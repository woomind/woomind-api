/**
 * Model for AccessToken entity.
 *
 * AccessToken should be understood in scope of OAuth2 :
 * A token that allows a user to access a resource via a specifically granted client.
 */

const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const baseModel = require('./base');

const AccessTokenSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  clientId: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    unique: true,
    required: true,
  },
  expirationDate: {
    type: Date,
    default: Date.now(),
  },
  scope: {
    type: [
      {
        type: String,
        trim: true,
      },
    ],
    required: true,
  },
});

AccessTokenSchema.plugin(timestamps);
AccessTokenSchema.static(baseModel.statics);
const AccessToken = mongoose.model('AccessToken', AccessTokenSchema);

AccessToken.removeExpired = async () => {
  await AccessToken.deleteMultiple({ expirationDate: { $lte: new Date() } });
};

module.exports = AccessToken;
